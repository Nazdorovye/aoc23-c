#include <openssl/crypto.h>
#include <openssl/evp.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define HASH_MAKE_STRING
#define KEY_MAX_LEN 32

void algorithm(FILE* file, ui64* results) {
    i8 key[ KEY_MAX_LEN ] = {0};

    fgets(key, KEY_MAX_LEN, file);

    ui8* hash = calloc(sizeof(ui8), EVP_MAX_MD_SIZE);
    for ( ui64 postfix = 0; postfix < UINT64_MAX; postfix++ ) {
        i8 newKey[ KEY_MAX_LEN ] = {0};
        ui32 length              = sprintf(newKey, "%s%ld", key, postfix);

        EVP_MD_CTX* mdCtx = EVP_MD_CTX_new();
        EVP_DigestInit_ex(mdCtx, EVP_md5(), NULL);

        EVP_DigestUpdate(mdCtx, (ui8*)newKey, length);

        EVP_DigestFinal_ex(mdCtx, hash, NULL);
        EVP_MD_CTX_destroy(mdCtx);

        if ( !results[ Part1 ] && !hash[ 0 ] && !hash[ 1 ] && !(hash[ 2 ] >> 4) ) {
            results[ Part1 ] = postfix;
        }
        if ( !results[ Part2 ] && !hash[ 0 ] && !hash[ 1 ] && !hash[ 2 ] ) {
            results[ Part2 ] = postfix;
        }
        if ( results[ Part1 ] && results[ Part2 ] )
            break;
    }
    free(hash);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 4, &algorithm);
    return EXIT_SUCCESS;
}
