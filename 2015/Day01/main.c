#include <stdint.h>
#include <stdlib.h>

#include "defines.h"

void algorithm(FILE* file, ui64* results) {
    ui64 position = 1;
    for ( i8 c = fgetc(file), entered = 0; c != EOF; c = fgetc(file), position++ ) {
        switch ( c ) {
            case '(': results[ Part1 ]++; break;
            case ')': results[ Part1 ]--; break;
            case EOF: return;
        }

        if ( results[ Part1 ] == UINT64_MAX && !entered ) {
            entered          = 1;
            results[ Part2 ] = position;
        }
    }
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 1, &algorithm);
    return EXIT_SUCCESS;
}
