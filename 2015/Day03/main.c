#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

typedef union _House {
    struct {
        bool single : 1;
        bool coop : 1;
    } split;
    ui8 data;
} House;

void algorithm(FILE* file, ui64* results) {
    ui16 x[ 3 ] = {INT16_MAX, INT16_MAX, INT16_MAX};
    ui16 y[ 3 ] = {INT16_MAX, INT16_MAX, INT16_MAX};
    i8 turn     = 0;

    House** map = (House**)calloc(UINT16_MAX, sizeof(House*));
    ui16* xs    = stackArrayInit(UINT16_MAX, sizeof(ui16), NULL);

    map[ x[ turn ] ] = (House*)calloc(UINT16_MAX, sizeof(House));
    stackArrayPushRval(xs, x[ turn ]);

    map[ x[ turn ] ][ y[ turn ] ].data = 255;
    results[ Part1 ]++;
    results[ Part2 ]++;
    for ( i8 c = fgetc(file); c != EOF; c = fgetc(file), turn ^= 1 ) {
        switch ( c ) {
            case '^':
                y[ 2 ]++;
                y[ turn ]++;
                break;
            case 'v':
                y[ 2 ]--;
                y[ turn ]--;
                break;
            case '<':
                x[ 2 ]--;
                x[ turn ]--;
                break;
            case '>':
                x[ 2 ]++;
                x[ turn ]++;
                break;
        }

        if ( map[ x[ turn ] ] == NULL ) {
            map[ x[ turn ] ] = (House*)calloc(UINT16_MAX, sizeof(House));
            stackArrayPushRval(xs, x[ turn ]);
        }
        if ( map[ x[ 2 ] ] == NULL ) {
            map[ x[ 2 ] ] = (House*)calloc(UINT16_MAX, sizeof(House));
            stackArrayPushRval(xs, x[ 2 ]);
        }

        results[ Part1 ] += !map[ x[ 2 ] ][ y[ 2 ] ].split.single;
        map[ x[ 2 ] ][ y[ 2 ] ].split.single = true;

        results[ Part2 ] += !map[ x[ turn ] ][ y[ turn ] ].split.coop;
        map[ x[ turn ] ][ y[ turn ] ].split.coop = true;
    }

    for ( ui16 x = 0; x < stackArrayGetHeader(xs)->length; x++ )
        free(map[ xs[ x ] ]);
    free(map);
    stackArrayFree(xs);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 3, &algorithm);
    return EXIT_SUCCESS;
}
