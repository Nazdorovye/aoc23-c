#include <stdint.h>
#include <stdlib.h>

#include "defines.h"

#define L 0
#define W 1
#define H 2

void algorithm(FILE* file, ui64* results) {
    i8* numReader = stackArrayInit(3, sizeof(i8), NULL); // additional entry for zero termination

    ui8 dims[ 3 ] = {0};
    ui8 dimIdx    = L;

    i8 c = 1;
    do {
        c = fgetc(file);
        if ( c == 'x' || c == '\n' || c == EOF ) {
            dims[ dimIdx ] = atoi(numReader);
            stackArrayClear(numReader);
            switch ( dimIdx ) {
                case (H): dimIdx = L; break;
                default:  dimIdx++;
            }
            if ( c == '\n' || c == EOF ) {
                ui64 args1[ 3 ] = {dims[ L ] * dims[ W ], dims[ W ] * dims[ H ], dims[ H ] * dims[ L ]};
                results[ Part1 ] += 2 * args1[ 0 ] + 2 * args1[ 1 ] + 2 * args1[ 2 ] + min(min(args1[ 0 ], args1[ 1 ]), args1[ 2 ]);

                ui64 side1 = min(dims[ L ], min(dims[ W ], dims[ H ]));
                ui64 side2 = side1 == dims[ L ] ? min(dims[ W ], dims[ H ])
                                                : (side1 == dims[ W ] ? min(dims[ L ], dims[ H ]) : min(dims[ W ], dims[ L ]));
                results[ Part2 ] += (side1 * 2) + (side2 * 2) + (dims[ L ] * dims[ W ] * dims[ H ]);
            }
            continue;
        }
        stackArrayPushRval(numReader, c);
    } while ( c != EOF );

    stackArrayFree(numReader);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 2, &algorithm);
    return EXIT_SUCCESS;
}
