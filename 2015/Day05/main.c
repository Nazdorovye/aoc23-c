#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

typedef union _Word {
    struct {
        i8 a;
        i8 b;
    } split;
    i16 data;
} Word;

typedef enum _ShittySubstring { ab = 25185, cd = 25699, pq = 29040, xy = 31096 } ShittySubstring;

static inline bool isVowel(i8 c) {
    switch ( c ) {
        case 'a':
        case 'A':
        case 'e':
        case 'E':
        case 'i':
        case 'I':
        case 'o':
        case 'O':
        case 'u':
        case 'U': return true;
        default:  return false;
    }
}

void algorithm(FILE* file, ui64* results) {
    i8 colCount = 1;
    for ( i8 c = fgetc(file); c != '\n' && c != EOF; c = fgetc(file), colCount++ ) {}

    rewind(file);

    i8* buffer = (i8*)calloc(colCount, sizeof(i8));

    while ( fgets(buffer, colCount, file) && getc(file) ) {
        ui8 vowels  = isVowel(buffer[ 0 ]);
        ui8 doubles = 0;
        bool rule3  = true;

        for ( ui8 i = 0, j = i + 1; rule3 && j < colCount; i++, j++ ) {
            vowels += isVowel(buffer[ j ]);        // rule 1
            doubles += buffer[ i ] == buffer[ j ]; // rule 2

            Word word = {.split = {buffer[ i ], buffer[ j ]}};
            switch ( word.data ) {
                case ab:
                case cd:
                case pq:
                case xy: rule3 = false;
            }
        }

        results[ Part1 ] += (vowels >= 3) && doubles && rule3;
    }

    free(buffer);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 5, &algorithm);
    return EXIT_SUCCESS;
}
