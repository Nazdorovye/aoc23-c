#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define BLINK_COUNT_1         25
#define BLINK_COUNT_2         75
#define BLINK_STONE_INIT_SIZE 1000

typedef struct _CacheEntry {
    ui64 value;
    ui64 stoneCount;
} CacheEntry;

CacheEntry** cache;

CacheEntry* findEntry(ui64 value, ui64 blinkCount) {
    for ( ui64 i = 0; i < stackArrayGetHeader(cache[ blinkCount - 1 ])->length; i++ ) {
        if ( cache[ blinkCount - 1 ][ i ].value == value ) {
            return &cache[ blinkCount - 1 ][ i ];
        }
    }
    return NULL;
}

static inline void addEntry(ui64 value, ui64 stoneCount, ui64 blinkCount) {
    stackArrayPushRval(cache[ blinkCount - 1 ], ((CacheEntry){value, stoneCount}));
}

ui64 doThings(ui64 input, ui64 blinks) {
    if ( blinks == 0 ) {
        return 1;
    }

    CacheEntry* entry = findEntry(input, blinks);
    if ( entry != NULL ) {
        return entry->stoneCount;
    }

    ui64 count = 0;
    if ( input == 0 ) {
        count += doThings(1, blinks - 1);
    } else {
        ui8 digits = digitCount(input);
        if ( digits % 2 == 0 ) {
            ui64 split = 1;
            for ( ui8 i = 0; i < digits / 2; i++, split *= 10 ) {}

            count += doThings(input / split, blinks - 1);
            count += doThings(input % split, blinks - 1);
        } else {
            count += doThings(input * 2024, blinks - 1);
        }
    }

    addEntry(input, count, blinks);
    return count;
}

void algorithm(FILE* file, ui64* results) {
    i8* numReader   = stackArrayInit(20, sizeof(i8), NULL);
    ui64* numBuffer = stackArrayInit(10, sizeof(ui64), NULL);

    cache = stackArrayInit(BLINK_COUNT_2, sizeof(CacheEntry*), NULL);
    for ( ui64 i = 0; i < BLINK_COUNT_2; i++ ) {
        stackArrayPushRval(cache, stackArrayInit(BLINK_STONE_INIT_SIZE, sizeof(CacheEntry), NULL));
    }

    for ( i8 c = fgetc(file);; c = fgetc(file) ) {
        if ( isDigit(c) ) {
            stackArrayPushRval(numReader, c);
            continue;
        }
        if ( c == ' ' || c == '\n' || c == EOF ) {
            stackArrayPushRval(numBuffer, atoll(numReader));
            stackArrayClear(numReader);
        }
        if ( c == EOF )
            break;
    }

    for ( ui32 i = 0; i < stackArrayGetHeader(numBuffer)->length; i++ ) {
        results[ Part1 ] += doThings(numBuffer[ i ], BLINK_COUNT_1);
    }

    for ( ui32 i = 0; i < stackArrayGetHeader(numBuffer)->length; i++ ) {
        results[ Part2 ] += doThings(numBuffer[ i ], BLINK_COUNT_2);
    }

    for ( ui64 i = 0; i < BLINK_COUNT_2; i++ ) {
        stackArrayFree(cache[ i ]);
    }
    stackArrayFree(cache);

    stackArrayFree(numBuffer);
    stackArrayFree(numReader);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 11, &algorithm);
    return EXIT_SUCCESS;
}
