#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

typedef union _Delta {
    struct {
        i8 x, y;
    } split;
    ui16 data;
} Delta;

typedef union _Position {
    struct {
        ui32 x, y;
    } split;
    ui64 data;
} Position;

typedef struct _History {
    Position pos;
    i8 symbol;
} History;

typedef enum _Direction { U = 65280, D = 256, L = 255, R = 1 } Direction;

static ui64 mapCols, mapRows;

static Position move1(i8 map[][ mapCols ], Position from, Delta delta) {
    Position to = (Position){.split = {from.split.x + delta.split.x, from.split.y + delta.split.y}};
    switch ( map[ to.split.y ][ to.split.x ] ) {
        case '.': {
            map[ to.split.y ][ to.split.x ] = map[ from.split.y ][ from.split.x ];
            return to;
        }
        case 'O': {
            Position ret = move1(map, to, delta);
            if ( ret.data == to.data ) {
                return from;
            }
            map[ to.split.y ][ to.split.x ] = map[ from.split.y ][ from.split.x ];
            return to;
        }
        default: return from;
    }
}

static void addHistoryEntry(i8 map[][ mapCols * 2 ], History* history, Position from, Position from2, Position to, Position to2) {
    if ( history == NULL ) {
        return;
    }
    stackArrayPushRval(history, ((History){from, map[ from.split.y ][ from.split.x ]}));
    stackArrayPushRval(history, ((History){from2, map[ from2.split.y ][ from2.split.x ]}));
    stackArrayPushRval(history, ((History){to, map[ to.split.y ][ to.split.x ]}));
    stackArrayPushRval(history, ((History){to2, map[ to2.split.y ][ to2.split.x ]}));
}

Position move2(i8 map[][ mapCols * 2 ], Position from, Delta delta, History* history) {
    Position to = (Position){.split = {from.split.x + delta.split.x, from.split.y + delta.split.y}};

    i8* curr = &map[ from.split.y ][ from.split.x ];
    i8* next = &map[ to.split.y ][ to.split.x ];

    if ( delta.data < D ) { // horizontal
        switch ( *next ) {
            case '.': {
                *next = *curr;
                return to;
            }
            case '[':
            case ']': {
                Position toNext = (Position){.split = {to.split.x + delta.split.x, to.split.y}};
                Position ret    = move2(map, toNext, delta, NULL);
                if ( ret.data == toNext.data ) {
                    return from;
                }

                map[ toNext.split.y ][ toNext.split.x ] = *next;
                *next                                   = *curr;
                return to;
            }
            default: return from;
        }
    }

    if ( *curr == '@' ) {
        switch ( *next ) {
            case '.': {
                *next = *curr;
                return to;
            }
            case '[':
            case ']': {
                Position ret = move2(map, to, delta, NULL);
                if ( ret.data == to.data ) {
                    return from;
                }

                *next = *curr;
                return to;
            }
            default: return from;
        }
    }

    Position from2;
    Position to2;

    if ( *curr == '[' ) {
        from2 = (Position){.split = {from.split.x + 1, from.split.y}};
        to2   = (Position){.split = {to.split.x + 1, to.split.y}};
    } else {
        from2 = (Position){.split = {from.split.x - 1, from.split.y}};
        to2   = (Position){.split = {to.split.x - 1, to.split.y}};
    }

    i8* curr2 = &map[ from2.split.y ][ from2.split.x ];
    i8* next2 = &map[ to2.split.y ][ to2.split.x ];

    if ( *next == '.' && *next2 == '.' ) {
        addHistoryEntry(map, history, from, from2, to, to2);
        *next  = *curr;
        *next2 = *curr2;
        *curr  = '.';
        *curr2 = '.';
        return to;
    }

    if ( *next == '#' || *next2 == '#' ) {
        return from;
    }

    if ( *next == *curr && *next2 == *curr2 ) {
        Position ret = move2(map, to, delta, history);
        if ( ret.data == to.data ) {
            return from;
        }
        addHistoryEntry(map, history, from, from2, to, to2);

        *next  = *curr;
        *next2 = *curr2;
        *curr  = '.';
        *curr2 = '.';
        return to;
    }

    if ( *next == *curr2 && *next2 == *curr ) {
        History* _history;
        if ( history != NULL ) {
            _history = history;
        } else {
            _history = stackArrayInit(100, sizeof(History), NULL);
        }

        Position ret2 = move2(map, to2, delta, _history);
        if ( ret2.data == to2.data ) {
            if ( history == NULL ) {
                for ( History pop; stackArrayPop(_history, &pop); ) {
                    map[ pop.pos.split.y ][ pop.pos.split.x ] = pop.symbol;
                }
                stackArrayFree(_history);
            }
            return from;
        }

        Position ret = move2(map, to, delta, _history);
        if ( ret.data == to.data ) {
            if ( history == NULL ) {
                for ( History pop; stackArrayPop(_history, &pop); ) {
                    map[ pop.pos.split.y ][ pop.pos.split.x ] = pop.symbol;
                }
                stackArrayFree(_history);
            }

            return from;
        }
        if ( history == NULL )
            stackArrayFree(_history);
        addHistoryEntry(map, history, from, from2, to, to2);

        *next  = *curr;
        *next2 = *curr2;
        *curr  = '.';
        *curr2 = '.';
        return to;
    }

    if ( *next == *curr2 ) {
        Position ret = move2(map, to, delta, history);
        if ( ret.data == to.data ) {
            return from;
        }
        addHistoryEntry(map, history, from, from2, to, to2);

        *next  = *curr;
        *next2 = *curr2;
        *curr  = '.';
        *curr2 = '.';
        return to;
    }

    if ( *next2 == *curr ) {
        Position ret = move2(map, to2, delta, history);
        if ( ret.data == to2.data ) {
            return from;
        }
        addHistoryEntry(map, history, from, from2, to, to2);

        *next  = *curr;
        *next2 = *curr2;
        *curr  = '.';
        *curr2 = '.';
        return to;
    }

    return from;
}

void algorithm(FILE* file, ui64* results) {
    mapCols = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file), mapCols++ ) {}

    mapRows             = 1;
    Position robot[ 2 ] = {0};
    ui64 col            = 1;
    for ( i8 c = fgetc(file), prev_c; c != '\n' || prev_c != '\n'; prev_c = c, c = fgetc(file) ) {
        switch ( c ) {
            case '\n':
                mapRows++;
                col = 0;
                continue;
            case '@':
                robot[ Part1 ].split.x = col;
                robot[ Part1 ].split.y = mapRows;
                robot[ Part2 ].split.x = col * 2;
                robot[ Part2 ].split.y = mapRows;
                break;
        }
        col++;
    }

    rewind(file);

    i8 map1[ mapRows ][ mapCols ];
    i8 map2[ mapRows ][ mapCols * 2 ];
    for ( ui64 r = 0; r < mapRows; r++ ) {
        for ( ui64 c = 0, c2 = 0; c < mapCols; c++, c2 += 2 ) {
            map1[ r ][ c ] = fgetc(file);
            switch ( map1[ r ][ c ] ) {
                case '.':
                case '#':
                    map2[ r ][ c2 ]     = map1[ r ][ c ];
                    map2[ r ][ c2 + 1 ] = map1[ r ][ c ];
                    break;
                case '@':
                    map2[ r ][ c2 ]     = map1[ r ][ c ];
                    map2[ r ][ c2 + 1 ] = '.';
                    break;
                case 'O':
                    map2[ r ][ c2 ]     = '[';
                    map2[ r ][ c2 + 1 ] = ']';
                    break;
            }
        }
        fgetc(file); // skip newline symbol
    }

    ui64 i = 0, f = 0;
    for ( i8 c = fgetc(file); c != EOF; c = fgetc(file), i++ ) {
        Delta delta = {0};
        switch ( c ) {
            case '^': delta.data = U; break;
            case 'v': delta.data = D; break;
            case '<': delta.data = L; break;
            case '>': delta.data = R; break;
            default:  continue;
        }

        Position new = move1(map1, robot[ Part1 ], delta);
        if ( new.data != robot[ Part1 ].data ) {
            map1[ robot[ Part1 ].split.y ][ robot[ Part1 ].split.x ] = '.';
            robot[ Part1 ]                                           = new;
        }

        new = move2(map2, robot[ Part2 ], delta, NULL);
        if ( new.data != robot[ Part2 ].data ) {
            map2[ robot[ Part2 ].split.y ][ robot[ Part2 ].split.x ] = '.';
            robot[ Part2 ]                                           = new;
        }
    }

    for ( ui64 r = 1; r < mapRows; r++ ) {
        for ( ui64 c = 1; c < mapCols * 2 - 2; c++ ) {
            if ( c < mapCols - 1 && map1[ r ][ c ] == 'O' ) {
                results[ Part1 ] += 100 * r + c;
            }
            if ( map2[ r ][ c ] == '[' ) {
                results[ Part2 ] += 100 * r + c;
            }
        }
    }
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 15, &algorithm);
    return EXIT_SUCCESS;
}

// low 1475438
