#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

typedef union _Coord {
    struct {
        ui8 row;
        ui8 col;
    } split;
    ui16 value;
} Coord;

static i8** grid;
static Coord* scores;
static ui16 lastScore;
static ui64 colCount;
static ui64 rowCount;

static void isValid(Coord coord, i8 prevSym, ui64* rating) {
    if ( coord.split.col < 0 || coord.split.row < 0 || coord.split.col >= colCount || coord.split.row >= rowCount ) {
        return;
    }

    i8 sym = grid[ coord.split.row ][ coord.split.col ];
    if ( sym - prevSym != 1 && prevSym != 's' ) {
        return;
    }

    if ( sym == '9' ) {
        (*rating)++;
        for ( ui64 i = 0; i < lastScore; i++ ) {
            if ( scores[ i ].value == coord.value ) {
                return;
            }
        }
        scores[ lastScore++ ] = coord;
        return;
    }

    isValid((Coord){coord.split.row + 1, coord.split.col}, sym, rating);
    isValid((Coord){coord.split.row, coord.split.col + 1}, sym, rating);
    isValid((Coord){coord.split.row - 1, coord.split.col}, sym, rating);
    isValid((Coord){coord.split.row, coord.split.col - 1}, sym, rating);
}

void algorithm(FILE* file, ui64* results) {
    colCount = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file), colCount++ ) {}

    fseek(file, 0, SEEK_END);
    rowCount = (ftell(file) + 1) / (colCount + 1);

    rewind(file);

    grid = malloc(sizeof(ui8*) * rowCount);
    for ( ui64 r = 0; r < rowCount; r++ ) {
        grid[ r ] = calloc(colCount, sizeof(ui8));
        fgets(grid[ r ], colCount + 1, file);
        fgetc(file); // skip newline symbol
    }

    scores = calloc(324, sizeof(Coord));
    for ( ui64 r = 0; r < rowCount; r++ ) {
        for ( ui64 c = 0; c < colCount; c++ ) {
            if ( grid[ r ][ c ] != '0' )
                continue;

            lastScore = 0;
            isValid((Coord){r, c}, 's', &results[ Part2 ]);
            results[ Part1 ] += lastScore;
        }
    }

    for ( ui64 r = 0; r < rowCount; r++ ) {
        free(grid[ r ]);
    }
    free(grid);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 10, &algorithm);
    return EXIT_SUCCESS;
}
