#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define MAP_W             101
#define MAP_H             103
#define ROBOT_INIT_COUNT  500
#define SIMULATION_CYCLES 100

typedef union _Robot {
    struct {
        i8 px, py, vx, vy;
    } split;
    i8 data[ 4 ];
} Robot;

typedef enum _ReadMode { px, py, vx, vy } ReadMode;

inline static Robot timeTravel(Robot* robot, ui64 cycles) {
    return (Robot){
        .split.px = (robot->split.px + cycles * (robot->split.vx + MAP_W)) % MAP_W,
        .split.py = (robot->split.py + cycles * (robot->split.vy + MAP_H)) % MAP_H,
        .split.vx = robot->split.vx,
        .split.vy = robot->split.vy,
    };
}

void algorithm(FILE* file, ui64* results) {
    i8* numReader     = stackArrayInit(20, sizeof(i8), NULL);
    ReadMode readMode = px;
    i8 negate         = 1;

    Robot* robots[ 2 ]  = {stackArrayInit(ROBOT_INIT_COUNT, sizeof(Robot), NULL),
                           stackArrayInit(ROBOT_INIT_COUNT, sizeof(Robot), NULL)};
    Robot* current[ 2 ] = {stackArrayPushRval(robots[ Part1 ], ((Robot){0, 0, 0, 0})),
                           stackArrayPushRval(robots[ Part2 ], ((Robot){0, 0, 0, 0}))};

    ui64 quadrants[ 2 ][ 2 ] = {0};

    ui8 qw = (MAP_W - 1) / 2;
    ui8 qh = (MAP_H - 1) / 2;

    for ( i8 c = fgetc(file);; c = fgetc(file) ) {
        if ( isDigit(c) ) {
            stackArrayPushRval(numReader, c);
            continue;
        }
        if ( c == '-' ) {
            negate = -1;
            continue;
        }
        if ( c == ',' || c == ' ' || c == '\n' || c == EOF ) {
            current[ Part1 ]->data[ readMode ] = atoi(numReader) * negate;
            readMode == vy ? readMode = px : readMode++;
            stackArrayClear(numReader);
            negate = 1;
            if ( c == '\n' || c == EOF ) {
                *current[ Part2 ] = *current[ Part1 ];
                *current[ Part1 ] = timeTravel(current[ Part1 ], SIMULATION_CYCLES);

                // non-median
                if ( current[ Part1 ]->split.px != qw && current[ Part1 ]->split.py != qh )
                    quadrants[ current[ Part1 ]->split.py > qh ][ current[ Part1 ]->split.px > qw ]++;

                current[ Part1 ] = stackArrayPushRval(robots[ Part1 ], ((Robot){0, 0, 0, 0}));
                current[ Part2 ] = stackArrayPushRval(robots[ Part2 ], ((Robot){0, 0, 0, 0}));
            }
            if ( c == EOF ) {
                stackArrayPop(robots[ Part1 ], NULL);
                stackArrayPop(robots[ Part2 ], NULL);
                break;
            }
        }
    }

    results[ Part1 ] = quadrants[ 0 ][ 0 ] * quadrants[ 0 ][ 1 ] * quadrants[ 1 ][ 0 ] * quadrants[ 1 ][ 1 ];

    /**
        Assumption is that xmas tree located directly in the middle of the map
        Thus, a lot of robots are located on the medians, so safety factor must be the lowest
     */

    ui64 minSf      = UINT64_MAX;
    ui64 minSfIndex = 0;

    for ( ui64 i = 0; i < MAP_H * MAP_W; i++ ) {
        quadrants[ 0 ][ 0 ] = 0;
        quadrants[ 0 ][ 1 ] = 0;
        quadrants[ 1 ][ 0 ] = 0;
        quadrants[ 1 ][ 1 ] = 0;

        for ( ui64 r = 0; r < stackArrayGetHeader(robots[ Part2 ])->length; r++ ) {
            Robot newRobot = timeTravel(&robots[ Part2 ][ r ], i);
            // non-median
            if ( newRobot.split.px != qw && newRobot.split.py != qh )
                quadrants[ newRobot.split.py > qh ][ newRobot.split.px > qw ]++;
        }

        ui64 sf = quadrants[ 0 ][ 0 ] * quadrants[ 0 ][ 1 ] * quadrants[ 1 ][ 0 ] * quadrants[ 1 ][ 1 ];
        if ( sf < minSf ) {
            minSf      = sf;
            minSfIndex = i;
        }
    }

    results[ Part2 ] = minSfIndex;

    for ( ui64 r = 0; r < stackArrayGetHeader(robots[ Part2 ])->length; r++ ) {
        robots[ Part2 ][ r ] = timeTravel(&robots[ Part2 ][ r ], minSfIndex);
    }

    // draw this fucker
    // for ( ui8 h = 0; h < MAP_H; h++ ) {
    //     for ( ui8 w = 0; w < MAP_W; w++ ) {
    //         ui32 count = 0;
    //         for ( ui64 r = 0; r < stackArrayGetHeader(robots[ Part2 ])->length; r++ ) {
    //             current[ Part2 ] = &robots[ Part2 ][ r ];
    //             if ( current[ Part2 ]->split.px == w && current[ Part2 ]->split.py == h )
    //                 count++;
    //         }
    //         if ( count > 0 )
    //             printf("#");
    //         else
    //             printf(" ");
    //     }
    //     printf("\n");
    // }

    stackArrayFree(robots[ Part1 ]);
    stackArrayFree(robots[ Part2 ]);
    stackArrayFree(numReader);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 14, &algorithm);
    return EXIT_SUCCESS;
}
