#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define NUM_CAP 30

typedef enum _Operator { mul, add } Operator;

inline static i64 concat(i64 a, i64 b) {
    ui64 pow = 1;
    while ( b >= pow )
        pow *= 10;
    return a * pow + b;
}

static bool solve(ui64* numbers, ui64 length, ui64 current, ui64 index) {
    if ( current > numbers[ 0 ] ) {
        return false;
    }
    if ( index >= length ) {
        return current == numbers[ 0 ];
    }

    return solve(numbers, length, current + numbers[ index ], index + 1)   ? true
           : solve(numbers, length, current * numbers[ index ], index + 1) ? true
                                                                           : false;
}

static bool solveConcat(ui64* numbers, ui64 length, ui64 current, ui64 index) {
    if ( current > numbers[ 0 ] ) {
        return false;
    }
    if ( index >= length ) {
        return current == numbers[ 0 ];
    }

    return solveConcat(numbers, length, current + numbers[ index ], index + 1)          ? true
           : solveConcat(numbers, length, current * numbers[ index ], index + 1)        ? true
           : solveConcat(numbers, length, concat(current, numbers[ index ]), index + 1) ? true
                                                                                        : false;
}

void algorithm(FILE* file, ui64* results) {
    ui64* numbers = stackArrayInit(NUM_CAP, sizeof(ui64), NULL);
    i8* numReader = stackArrayInit(20, sizeof(i8), NULL);

    for ( i8 c = fgetc(file);; c = fgetc(file) ) {
        if ( isDigit(c) ) {
            stackArrayPushRval(numReader, c);
            continue;
        }
        if ( c == ':' )
            c = fgetc(file); // skip semicol
        if ( c == ' ' || c == '\n' || c == EOF ) {
            stackArrayPushRval(numbers, atol(numReader));
            stackArrayClear(numReader);
        }
        if ( c == '\n' || c == EOF ) {
            results[ Part1 ] += numbers[ 0 ] * solve(numbers, stackArrayGetHeader(numbers)->length, 0, 1);
            results[ Part2 ] += numbers[ 0 ] * solveConcat(numbers, stackArrayGetHeader(numbers)->length, 0, 1);
        }
        if ( c == '\n' )
            stackArrayClear(numbers);
        if ( c == EOF )
            break;
    }

    stackArrayFree(numbers);
    stackArrayFree(numReader);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 7, &algorithm);
    return EXIT_SUCCESS;
}
