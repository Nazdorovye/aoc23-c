#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

typedef union _Position {
    struct {
        ui8 x, y;
    };
    ui16 hash;
} Position;

typedef struct _Direction {
    i8 x : 2, y : 2;
} Direction;

typedef union _SpatialData {
    struct {
        Direction dir;
        Position pos;
    };
    ui32 hash;
} SpatialData;

typedef struct _QueueItem {
    ui32 cost;
    SpatialData spatial;
} QueueItem;

#define BI_TREE_LEFT(x)   (2 * (x) + 1)
#define BI_TREE_RIGHT(x)  (2 * (x) + 2)
#define BI_TREE_PARENT(x) ((x) / 2)

void pqHeapify(void* pq, ui64 index) {
    ui64 leftIdx  = BI_TREE_LEFT(index);
    ui64 rightIdx = BI_TREE_RIGHT(index);
    ui64 largestIdx;
    ui64 length = stackArrayGetHeader(pq)->length;

    // Left child exists, compare it with its parent
    if ( leftIdx < length && ((QueueItem*)(pq))[ leftIdx ].cost < ((QueueItem*)(pq))[ index ].cost ) {
        largestIdx = leftIdx;
    } else {
        largestIdx = index;
    }

    // Right child exists, compare it with largest element
    if ( rightIdx < length && ((QueueItem*)(pq))[ rightIdx ].cost < ((QueueItem*)(pq))[ largestIdx ].cost ) {
        largestIdx = rightIdx;
    }

    // Largest element must be defined at this point
    QueueItem temp;
    if ( largestIdx != index ) {
        temp                             = ((QueueItem*)(pq))[ index ];
        ((QueueItem*)(pq))[ index ]      = ((QueueItem*)(pq))[ largestIdx ];
        ((QueueItem*)(pq))[ largestIdx ] = temp;
        pqHeapify(pq, largestIdx);
    }
}

void pqPush(void* pq, QueueItem value) {
    ui64 i = stackArrayGetHeader(pq)->length;
    stackArrayPushRval(pq, value);

    while ( i > 0 && ((QueueItem*)(pq))[ i ].cost < ((QueueItem*)(pq))[ BI_TREE_PARENT(i) ].cost ) {
        QueueItem temp                          = ((QueueItem*)(pq))[ i ];
        ((QueueItem*)(pq))[ i ]                 = ((QueueItem*)(pq))[ BI_TREE_PARENT(i) ];
        ((QueueItem*)(pq))[ BI_TREE_PARENT(i) ] = temp;

        i = BI_TREE_PARENT(i);
    }
}

void pqPop(void* pq, QueueItem* out) {
    if ( stackArrayGetHeader(pq)->length == 0 ) {
        return;
    }

    if ( stackArrayGetHeader(pq)->length == 1 ) {
        stackArrayPop(pq, out);
        return;
    }

    *out = ((QueueItem*)(pq))[ 0 ];
    stackArrayPop(pq, &((QueueItem*)(pq))[ 0 ]);
    pqHeapify(pq, 0);
}

void algorithm(FILE* file, ui64* results) {
    ui64 mapCols = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file), mapCols++ ) {}

    ui64 mapRows = 1;
    ui64 col     = 0;
    Position start;
    for ( i8 c = fgetc(file), run = true; run; c = fgetc(file) ) {
        switch ( c ) {
            case '\n':
            case EOF:
                mapRows++;
                col = 0;
                run = (c != EOF);
                continue;
            case 'S':
                start.x = col;
                start.y = mapRows;
                break;
        }
        col++;
    }

    rewind(file);

    i8 map[ mapRows ][ mapCols ];
    for ( ui64 r = 0; r < mapRows; r++ ) {
        for ( ui64 c = 0; c < mapCols; c++ ) {
            map[ r ][ c ] = fgetc(file);
        }
        fgetc(file); // skip newline symbol
    }

    ui32 bestCost = UINT32_MAX;

    ui32 maxHash      = (SpatialData){.dir = {0, -1}, .pos = {mapCols, mapRows}}.hash;
    ui32* lowestCosts = calloc(maxHash, sizeof(ui32));
    memset(lowestCosts, UINT8_MAX, sizeof(ui32) * maxHash);

    ui32** history                       = stackArrayInit(maxHash, sizeof(ui32*), NULL);
    stackArrayGetHeader(history)->length = maxHash;

    QueueItem* pq       = stackArrayInit(mapCols * mapRows, sizeof(QueueItem), NULL);
    QueueItem startItem = ((QueueItem){0, .spatial.pos = start, .spatial.dir = {1, 0}});
    pqPush(pq, startItem);

    ui32* endStateHashes = stackArrayInit(64, sizeof(ui32), NULL);

    while ( stackArrayGetHeader(pq)->length > 0 ) {
        QueueItem item;
        pqPop(pq, &item);

        if ( item.cost > lowestCosts[ item.spatial.hash ] ) {
            continue;
        }

        lowestCosts[ item.spatial.hash ] = item.cost;

        if ( map[ item.spatial.pos.y ][ item.spatial.pos.x ] == 'E' ) {
            if ( item.cost > bestCost ) {
                break;
            }
            bestCost = item.cost;

            bool seen = false;
            for ( ui8 i = 0; i < stackArrayGetHeader(endStateHashes)->length; i++ ) {
                if ( endStateHashes[ i ] == item.spatial.hash ) {
                    seen = true;
                    break;
                }
            }

            if ( !seen ) {
                stackArrayPushRval(endStateHashes, item.spatial.hash);
            }
        }

        QueueItem next = (QueueItem){
            item.cost + 1, .spatial = {.pos = {item.spatial.pos.x + item.spatial.dir.x, item.spatial.pos.y + item.spatial.dir.y},
                                       .dir = {.x = item.spatial.dir.x, .y = item.spatial.dir.y}}};
        if ( map[ next.spatial.pos.y ][ next.spatial.pos.x ] != '#' && next.cost <= lowestCosts[ next.spatial.hash ] ) {
            if ( history[ next.spatial.hash ] == NULL ) {
                history[ next.spatial.hash ] = stackArrayInit(10, sizeof(ui32), NULL);
            }
            if ( next.cost < lowestCosts[ next.spatial.hash ] ) {
                stackArrayClear(history[ next.spatial.hash ]);
                lowestCosts[ next.spatial.hash ] = next.cost;
            }
            stackArrayPushRval(history[ next.spatial.hash ], item.spatial.hash);
            pqPush(pq, next);
        }

        next =
            (QueueItem){item.cost + 1000, .spatial = {.pos = item.spatial.pos, .dir = {-item.spatial.dir.y, item.spatial.dir.x}}};
        if ( map[ next.spatial.pos.y ][ next.spatial.pos.x ] != '#' && next.cost <= lowestCosts[ next.spatial.hash ] ) {
            if ( history[ next.spatial.hash ] == NULL ) {
                history[ next.spatial.hash ] = stackArrayInit(10, sizeof(ui32), NULL);
            }
            if ( next.cost < lowestCosts[ next.spatial.hash ] ) {
                stackArrayClear(history[ next.spatial.hash ]);
                lowestCosts[ next.spatial.hash ] = next.cost;
            }
            stackArrayPushRval(history[ next.spatial.hash ], item.spatial.hash);
            pqPush(pq, next);
        }

        next =
            (QueueItem){item.cost + 1000, .spatial = {.pos = item.spatial.pos, .dir = {item.spatial.dir.y, -item.spatial.dir.x}}};
        if ( map[ next.spatial.pos.y ][ next.spatial.pos.x ] != '#' && next.cost <= lowestCosts[ next.spatial.hash ] ) {
            if ( history[ next.spatial.hash ] == NULL ) {
                history[ next.spatial.hash ] = stackArrayInit(10, sizeof(ui32), NULL);
            }
            if ( next.cost < lowestCosts[ next.spatial.hash ] ) {
                stackArrayClear(history[ next.spatial.hash ]);
                lowestCosts[ next.spatial.hash ] = next.cost;
            }

            stackArrayPushRval(history[ next.spatial.hash ], item.spatial.hash);
            pqPush(pq, next);
        }
    }

    results[ Part1 ] = bestCost;

    ui32* seenHashes = stackArrayInit(64, sizeof(ui32), NULL);
    ui16* uniquePos  = stackArrayInit(64, sizeof(ui16), NULL);
    results[ Part2 ] = 1;

    for ( ui32 i = 0; i < stackArrayGetHeader(endStateHashes)->length; i++ ) {
        ui32 prevHash = endStateHashes[ i ];
        if ( history[ prevHash ] == NULL ) {
            continue;
        }
        for ( i32 j = stackArrayGetHeader(history[ prevHash ])->length - 1; j >= 0; j-- ) {
            bool seen = false;
            for ( ui32 k = 0; k < stackArrayGetHeader(seenHashes)->length; k++ ) {
                if ( seenHashes[ k ] == history[ prevHash ][ j ] ) {
                    seen = true;
                    break;
                }
            }

            if ( !seen ) {
                stackArrayPushRval(seenHashes, history[ prevHash ][ j ]);
                stackArrayPushRval(endStateHashes, history[ prevHash ][ j ]);

                for ( i32 l = stackArrayGetHeader(uniquePos)->length - 1; l >= 0; l-- ) {
                    if ( uniquePos[ l ] == ((SpatialData)history[ prevHash ][ j ]).pos.hash ) {
                        seen = true;
                        break;
                    }
                }

                if ( !seen ) {
                    stackArrayPushRval(uniquePos, ((SpatialData)history[ prevHash ][ j ]).pos.hash);
                    results[ Part2 ] += 1;
                }
            }
        }
    }

    stackArrayFree(uniquePos);
    stackArrayFree(seenHashes);
    stackArrayFree(endStateHashes);
    for ( ui32 i = 0; i < stackArrayGetHeader(history)->length; i++ ) {
        if ( history[ i ] != NULL ) {
            stackArrayFree(history[ i ]);
        }
    }
    stackArrayFree(history);
    free(lowestCosts);
    stackArrayFree(pq);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 16, &algorithm);
    return EXIT_SUCCESS;
}
