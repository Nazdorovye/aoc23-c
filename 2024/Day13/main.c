#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define tokenPriceA 3
#define tokenPriceB 1
#define offset      10000000000000
#define EPSILON     0.000001

typedef enum _ReadTurn { ax, ay, bx, by, x, y } ReadTurn;

typedef struct _Result {
    ui64 A, B;
} Result;

static inline Result equate(i32 ax, i32 ay, i32 bx, i32 by, i64 x, i64 y) {
    f64 B = (f64)(ax * y - ay * x) / (f64)(ax * by - ay * bx);
    if ( fabs(B - round(B)) >= EPSILON )
        return (Result){0};

    f64 A = (x - bx * B) / ax;
    if ( fabs(A - round(A)) < EPSILON )
        return (Result){(ui64)A, (ui64)B};

    return (Result){0};
}

void algorithm(FILE* file, ui64* results) {
    ui64 numbers[ 6 ] = {0};

    i8* numReader = stackArrayInit(20, sizeof(i8), NULL);
    ReadTurn turn = y;

    for ( i8 c = fgetc(file), prev_c = c;; prev_c = c, c = fgetc(file) ) {
        if ( isDigit(c) ) {
            stackArrayPushRval(numReader, c);
            continue;
        }
        switch ( c ) {
            case '\n':
                if ( prev_c == '\n' )
                    break;
            case ',':
            case EOF:
                numbers[ turn ] = atoi(numReader);
                stackArrayClear(numReader);
                break;
            case 'X':
            case 'Y': turn == y ? turn = ax : turn++; break;
            default:  continue;
        }

        if ( c == '\n' && prev_c == '\n' || c == EOF ) {
            Result r = equate(numbers[ ax ], numbers[ ay ], numbers[ bx ], numbers[ by ], numbers[ x ], numbers[ y ]);
            if ( r.A > 0 && r.B > 0 )
                results[ Part1 ] += r.A * tokenPriceA + r.B * tokenPriceB;

            numbers[ x ] += offset;
            numbers[ y ] += offset;

            r = equate(numbers[ ax ], numbers[ ay ], numbers[ bx ], numbers[ by ], numbers[ x ], numbers[ y ]);
            if ( r.A > 0 && r.B > 0 )
                results[ Part2 ] += r.A * tokenPriceA + r.B * tokenPriceB;
        }

        if ( c == EOF )
            break;
    }

    stackArrayFree(numReader);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 13, &algorithm);
    return EXIT_SUCCESS;
}
