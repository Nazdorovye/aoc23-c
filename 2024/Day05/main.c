#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define UPDATE_CAPACITY      40
#define UPDATE_STRING_LENGTH 1024
#define MAX_NUMBER           100

#define isNumber(x) x >= '0' && x <= '9'

void algorithm(FILE* file, ui64* results) {
    bool rules[ MAX_NUMBER ][ MAX_NUMBER ] = {0};

    i8* numReader = stackArrayInit(4, sizeof(i8), NULL);
    for ( i8 c = fgetc(file), prev_c, left; !(prev_c == '\n' && c == '\n'); prev_c = c, c = fgetc(file) ) {
        if ( isNumber(c) ) {
            stackArrayPushRval(numReader, c);
            continue;
        }
        if ( c == '|' ) {
            left = atoi(numReader);
            stackArrayClear(numReader);
            continue;
        }
        if ( c == '\n' ) {
            rules[ left ][ atoi(numReader) ] = true;
            stackArrayClear(numReader);
        }
    }

    ui32* update = stackArrayInit(UPDATE_CAPACITY, sizeof(ui32), NULL);
    for ( i8 c = fgetc(file);; c = fgetc(file) ) {
        if ( isNumber(c) ) {
            stackArrayPushRval(numReader, c);
            continue;
        }
        if ( c == ',' || c == '\n' || c == EOF ) {
            stackArrayPushRval(update, atoi(numReader));
            stackArrayClear(numReader);
        }
        if ( c == '\n' || c == EOF ) {
            ui64 updateLength = stackArrayGetHeader(update)->length;
            bool part1        = true;
            for ( ui32 i = 0; i < updateLength - 1; i++ )
                for ( ui32 j = i + 1; j < updateLength; j++ ) {
                    if ( rules[ update[ j ] ][ update[ i ] ] ) {
                        part1 = false;

                        i32 temp    = update[ i ];
                        update[ i ] = update[ j ];
                        update[ j ] = temp;
                    }
                }

            results[ Part1 ] += update[ updateLength / 2 ] * part1;
            results[ Part2 ] += update[ updateLength / 2 ] * !part1;
            stackArrayClear(update);
        }
        if ( c == EOF )
            break;
    }

    stackArrayFree(numReader);
    stackArrayFree(update);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 5, &algorithm);
    return EXIT_SUCCESS;
}
