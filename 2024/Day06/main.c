#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define POS_CAP 1000

#define getHtIndex(prev_, curr_) (Hash){.split.curr = curr_, .split.prev = prev_}.idx;

typedef enum _MapNode { empty = '.', obstacle = '#', visited = 'X', up = '^', right = '>', down = 'v', left = '<' } MapNode;

typedef struct _Position {
    ui8 col;
    ui8 row;
} Position;

typedef struct _Actor {
    Position coords;
    i8 sym;
} Actor;

typedef union _Hash {
    struct _d {
        Position prev;
        Position curr;
    } split;
    ui32 idx;
} Hash;

typedef struct _Map {
    i8 part1;
    i8 part2;
} Map;

static ui64 colCount, rowCount;
static ui64* htRoute;
static ui32 htLength;
static ui64 signature[ 2 ];
static Map** map;

static inline void runLoopCheck(Actor* actor, ui64* result) {
    bool run = true;

    while ( run ) {
        i16 newCol = actor->coords.col, newRow = actor->coords.row;
        switch ( actor->sym ) {
            case up:    newRow--; break;
            case down:  newRow++; break;
            case left:  newCol--; break;
            case right: newCol++; break;
        }

        if ( newCol < 0 || newCol >= colCount || newRow < 0 || newRow >= rowCount ) {
            break;
        }

        switch ( map[ newRow ][ newCol ].part2 ) {
            case empty: {
                Position newCoords = {newCol, newRow};

                ui32 idx = getHtIndex(actor->coords, newCoords);
                if ( htRoute[ idx ] == signature[ Part1 ] || htRoute[ idx ] == signature[ Part2 ] ) {
                    (*result)++;
                    run = false;
                    break;
                }

                htRoute[ idx ] = signature[ Part2 ];
                actor->coords  = newCoords;
                break;
            }
            case obstacle: {
                switch ( actor->sym ) {
                    case up:    actor->sym = right; break;
                    case down:  actor->sym = left; break;
                    case right: actor->sym = down; break;
                    case left:  actor->sym = up; break;
                }
                break;
            }
        }
    }
}

void algorithm(FILE* file, ui64* results) {
    Actor actor[ 2 ]   = {0, 0};
    colCount           = 0;
    rowCount           = 0;
    signature[ Part1 ] = 1;
    signature[ Part2 ] = 2;

    ui64 row = 0, col = 0;
    for ( i8 c = ' '; c != EOF; rowCount++ )
        for ( c = fgetc(file), col = 0; c != '\n' && c != EOF; c = fgetc(file), col++ ) {
            if ( !col )
                colCount++;
            if ( c == empty || c == obstacle )
                continue;

            actor[ Part1 ].coords.col = col;
            actor[ Part1 ].coords.row = rowCount;
            actor[ Part1 ].sym        = c;
        }
    rewind(file);

    map = (Map**)calloc(rowCount, sizeof(Map*));
    for ( ui64 row = 0; row < rowCount; row++ ) {
        map[ row ] = (Map*)calloc(colCount, sizeof(Map));
        ui64 col   = 0;
        for ( i8 c = fgetc(file); c != '\n' && c != EOF; c = fgetc(file), col++ ) {
            map[ row ][ col ].part1 = c;
            map[ row ][ col ].part2 = c == empty || c == obstacle ? c : empty;
        }
    }

    htLength = getHtIndex(((Position){colCount - 1, rowCount - 1}), ((Position){colCount, rowCount}));
    htRoute  = (ui64*)calloc(htLength, sizeof(ui64));

    while ( true ) {
        i16 newCol = actor[ Part1 ].coords.col, newRow = actor[ Part1 ].coords.row;
        switch ( actor[ Part1 ].sym ) {
            case up:    newRow--; break;
            case down:  newRow++; break;
            case left:  newCol--; break;
            case right: newCol++; break;
        }

        if ( newCol < 0 || newCol >= colCount || newRow < 0 || newRow >= rowCount ) {
            results[ Part1 ]++;
            break;
        }

        switch ( map[ newRow ][ newCol ].part1 ) {
            case empty: {
                results[ Part1 ]++;
                map[ actor[ Part1 ].coords.row ][ actor[ Part1 ].coords.col ].part1 = visited;

                map[ newRow ][ newCol ].part2 = obstacle;

                actor[ Part2 ] = actor[ Part1 ];
                runLoopCheck(&actor[ Part2 ], &results[ Part2 ]);
                signature[ Part2 ]++;

                map[ newRow ][ newCol ].part2 = empty;

                Position newCoords    = {newCol, newRow};
                ui32 idx              = getHtIndex(actor[ Part1 ].coords, newCoords);
                htRoute[ idx ]        = signature[ Part1 ];
                actor[ Part1 ].coords = newCoords;

                break;
            }
            case visited: {
                map[ actor[ Part1 ].coords.row ][ actor[ Part1 ].coords.col ].part1 = visited;

                actor[ Part1 ].coords = (Position){newCol, newRow};
                break;
            }
            case obstacle: {
                switch ( actor[ Part1 ].sym ) {
                    case up:    actor[ Part1 ].sym = right; break;
                    case down:  actor[ Part1 ].sym = left; break;
                    case right: actor[ Part1 ].sym = down; break;
                    case left:  actor[ Part1 ].sym = up; break;
                }
                break;
            }
        }
    }

    free(htRoute);

    for ( ui64 row = 0; row < rowCount; row++ ) {
        free(map[ row ]);
    }
    free(map);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 6, &algorithm);
    return EXIT_SUCCESS;
}
