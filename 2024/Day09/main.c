#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

typedef enum _DataType { fileSize = 1, freeSize = 0 } DataType;

typedef struct _Buffer {
    i32 part1;
    i32 part2;
} Buffer;

typedef struct _Index {
    ui32 idx;
    ui8 len;
    ui8 taken;
} Index;

void algorithm(FILE* file, ui64* results) {
    ui32 bufferSize = 0;
    for ( i8 c = fgetc(file); c != EOF; c = fgetc(file) ) {
        bufferSize += ctoi8(c);
    }
    rewind(file);

    Buffer* buffer = calloc(bufferSize, sizeof(Buffer));
    ui32 bufferIdx = 0;

    Index* freeIndices = calloc(bufferSize, sizeof(Index));
    ui32 freeIdx       = 0;

    Index* fileIndices = calloc(bufferSize, sizeof(Index));
    ui32 fileIdx       = 0;

    ui64 id = 0;
    for ( i8 c = fgetc(file), sw = fileSize; c != EOF; c = fgetc(file), sw ^= 1, id += sw ) {
        if ( sw == fileSize ) {
            fileIndices[ fileIdx ].idx   = bufferIdx;
            fileIndices[ fileIdx++ ].len = ctoi8(c);
        }
        for ( ui8 n = ctoi8(c), i = 0; i < n; i++ ) {
            switch ( sw ) {
                case fileSize:
                    buffer[ bufferIdx ].part1   = id;
                    buffer[ bufferIdx++ ].part2 = id;
                    break;
                case freeSize:
                    freeIndices[ freeIdx ].idx   = bufferIdx;
                    freeIndices[ freeIdx++ ].len = n - i;
                    buffer[ bufferIdx ].part1    = -1;
                    buffer[ bufferIdx++ ].part2  = -1;
                    break;
            }
        }
    }

    for ( i32 i = 0, idx = bufferIdx; i < freeIdx && freeIndices[ i ].idx < idx - 1; i++ ) {
        while ( buffer[ freeIndices[ i ].idx ].part1 < 0 ) {
            buffer[ freeIndices[ i ].idx ].part1 = buffer[ --idx ].part1;
            buffer[ idx ].part1                  = -1;
        }
    }

    for ( i32 fileIndex = fileIdx - 1, freeIndex = 0; fileIndex >= 0 && fileIndices[ fileIndex ].idx > freeIndices[ freeIndex ].idx;
          fileIndex--, freeIndex                 = 0 ) {
        for ( ; freeIndex < freeIdx && freeIndices[ freeIndex ].idx < fileIndices[ fileIndex ].idx; freeIndex++ ) {
            if ( freeIndices[ freeIndex ].taken )
                continue;
            if ( freeIndices[ freeIndex ].len >= fileIndices[ fileIndex ].len ) {
                for ( i32 k = 0; k < fileIndices[ fileIndex ].len; k++ ) {
                    freeIndices[ freeIndex + k ].taken               = true;
                    buffer[ freeIndices[ freeIndex ].idx + k ].part2 = buffer[ fileIndices[ fileIndex ].idx + k ].part2;
                    buffer[ fileIndices[ fileIndex ].idx + k ].part2 = -1;
                }

                break;
            }
        }
    }

    for ( ui32 i = 0; i <= bufferIdx; i++ ) {
        results[ Part1 ] += (buffer[ i ].part1 >= 0) * buffer[ i ].part1 * i;
        results[ Part2 ] += (buffer[ i ].part2 >= 0) * buffer[ i ].part2 * i;
    }

    free(freeIndices);
    free(fileIndices);
    free(buffer);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 9, &algorithm);
    return EXIT_SUCCESS;
}
