#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define KEYW_LEN 4
static i8 keyword[ KEYW_LEN ] = "XMAS";

void algorithm(FILE* file, ui64* results) {
    ui64 colCount = 0;
    while ( fgetc(file) != '\n' )
        colCount++;

    fseek(file, 0, SEEK_END);
    ui64 rowCount = ftell(file) / colCount;

    rewind(file);

    i8** data = (i8**)malloc(rowCount * sizeof(i8*));
    for ( ui64 row = 0; row < rowCount; row++ ) {
        data[ row ] = (i8*)malloc(colCount * sizeof(i8));
        fgets(data[ row ], colCount + 1, file);
        fgetc(file); // skip newline
    }

    for ( ui64 row = 0; row < rowCount; row++ ) {
        for ( ui64 col = 0; col < colCount; col++ ) {
            if ( data[ row ][ col ] == 'A' && col > 0 && row > 0 && col < colCount - 1 && row < rowCount - 1 ) {
                if ( (data[ row + 1 ][ col + 1 ] == 'M' && data[ row - 1 ][ col - 1 ] == 'S') ||
                     (data[ row + 1 ][ col + 1 ] == 'S' && data[ row - 1 ][ col - 1 ] == 'M') ) {
                    if ( (data[ row + 1 ][ col - 1 ] == 'M' && data[ row - 1 ][ col + 1 ] == 'S') ||
                         (data[ row + 1 ][ col - 1 ] == 'S' && data[ row - 1 ][ col + 1 ] == 'M') ) {
                        results[ Part2 ]++;
                    }
                }
            }

            if ( data[ row ][ col ] == keyword[ 0 ] ) {
                for ( i8 dir = -1; dir <= 1; dir += 2 ) {
                    for ( i64 k = 1, ncol = col + k * dir; ncol >= 0 && ncol < colCount && k < KEYW_LEN; k++, ncol += dir ) {
                        if ( data[ row ][ ncol ] != keyword[ k ] )
                            break;
                        if ( k == KEYW_LEN - 1 )
                            results[ Part1 ]++;
                    }

                    for ( i64 k = 1, nrow = row + k * dir; nrow >= 0 && nrow < rowCount && k < KEYW_LEN; k++, nrow += dir ) {
                        if ( data[ nrow ][ col ] != keyword[ k ] )
                            break;
                        if ( k == KEYW_LEN - 1 )
                            results[ Part1 ]++;
                    }

                    for ( i64 k = 1, nrow = row + k * dir, ncol = col + k * dir;
                          nrow >= 0 && nrow < rowCount && ncol >= 0 && ncol < colCount && k < KEYW_LEN;
                          k++, ncol += dir, nrow += dir ) {
                        if ( data[ nrow ][ ncol ] != keyword[ k ] )
                            break;
                        if ( k == KEYW_LEN - 1 )
                            results[ Part1 ]++;
                    }

                    for ( i64 k = 1, nrow = row - k * dir, ncol = col + k * dir;
                          nrow >= 0 && nrow < rowCount && ncol >= 0 && ncol < colCount && k < KEYW_LEN;
                          k++, ncol += dir, nrow -= dir ) {
                        if ( data[ nrow ][ ncol ] != keyword[ k ] )
                            break;
                        if ( k == KEYW_LEN - 1 )
                            results[ Part1 ]++;
                    }
                }
            }
        }
    }

    for ( ui64 row = 0; row < rowCount; row++ )
        free(data[ row ]);
    free(data);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 4, &algorithm);
    return EXIT_SUCCESS;
}
