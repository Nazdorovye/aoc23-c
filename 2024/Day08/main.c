#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define INIT_CAP 20

typedef union _Coord {
    struct {
        ui8 row;
        ui8 col;
    } split;
    ui16 hash;
} Coord;

typedef union _Node {
    struct {
        bool part1;
        bool part2;
    } split;
    ui16 data;
} Node;

i8 hashKeyAntenna(i8 key) {
    return isDigit(key) ? key - 48 : isCapLetter(key) ? key - 7 : /* isLetter */ key - 6;
}

void algorithm(FILE* file, ui64* results) {
    i8 rowCount = 2, colCount = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file), colCount++ ) {}
    for ( i8 c = fgetc(file); c != EOF; c = fgetc(file), rowCount += c == '\n' ) {}

    rewind(file);

    Coord antennas[ 128 ][ 13000 ] = {0};
    ui16 nextAntennas[ 128 ]       = {0};
    Node antinodes[ 13000 ]        = {0};

    i16 row = 0, col = 0;
    for ( i8 c = fgetc(file); c != EOF; c = fgetc(file), col++ ) {
        if ( c == '.' ) {
            continue;
        }
        if ( c == '\n' ) {
            row++;
            col = -1;
            continue;
        }

        Coord current = {.split = {row, col}};

        results[ Part2 ] += !(antinodes[ current.hash ].split.part2);
        antinodes[ current.hash ].split.part2 = true;

        ui8 antennaHash                                          = hashKeyAntenna(c);
        antennas[ antennaHash ][ nextAntennas[ antennaHash ]++ ] = current;

        for ( i64 i = 0; i < nextAntennas[ antennaHash ] - 1; i++ ) {
            Coord check = antennas[ antennaHash ][ i ];

            i16 rowDiff = current.split.row - check.split.row;
            i16 colDiff = current.split.col - check.split.col;

            bool run[ 2 ] = {true, true};
            for ( i16 mul = 1; run[ 0 ] == true || run[ 1 ] == true; mul++ ) {
                i16 antiRow = check.split.row - rowDiff * mul;
                i16 antiCol = check.split.col - colDiff * mul;
                ui16 hash   = (Coord){.split = {antiRow, antiCol}}.hash;

                if ( antiRow >= 0 && antiRow < rowCount && antiCol >= 0 && antiCol < colCount ) {
                    if ( mul == 1 ) {
                        results[ Part1 ] += !(antinodes[ hash ].split.part1);
                        antinodes[ hash ].split.part1 = true;
                    }
                    results[ Part2 ] += !(antinodes[ hash ].split.part2);
                    antinodes[ hash ].split.part2 = true;
                } else {
                    run[ 0 ] = false;
                }

                antiRow = current.split.row + rowDiff * mul;
                antiCol = current.split.col + colDiff * mul;
                hash    = (Coord){.split = {antiRow, antiCol}}.hash;

                if ( antiRow >= 0 && antiRow < rowCount && antiCol >= 0 && antiCol < colCount ) {
                    if ( mul == 1 ) {
                        results[ Part1 ] += !(antinodes[ hash ].split.part1);
                        antinodes[ hash ].split.part1 = true;
                    }
                    results[ Part2 ] += !(antinodes[ hash ].split.part2);
                    antinodes[ hash ].split.part2 = true;
                } else {
                    run[ 1 ] = false;
                }
            }
        }
    }
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 8, &algorithm);
    return EXIT_SUCCESS;
}
