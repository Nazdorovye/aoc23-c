#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define DIGIT_CAP   3
#define NUMBERS_CAP 8

#define INC 1
#define DEC -1
#define NAN 0

static inline bool isSafe(ui8 a, ui8 b, i8* dir) {
    i8 diff = b - a;
    if ( diff == 0 || (diff > 0 && *dir == DEC) || (diff < 0 && *dir == INC) || diff > 3 || diff < -3 ) {
        return false;
    }
    *dir = diff > 0 ? INC : DEC;
    return true;
}

void algorithm(FILE* file, ui64* results) {
    i8* numReader = stackArrayInit(DIGIT_CAP, sizeof(i8), NULL);
    ui8* numbers  = stackArrayInit(NUMBERS_CAP, sizeof(ui8), NULL);

    bool safe = true;
    i8 dir    = NAN;
    for ( i8 c = fgetc(file), n = 0;; c = fgetc(file) ) {
        if ( c >= '0' && c <= '9' ) {
            stackArrayPushRval(numReader, c);
        } else {
            stackArrayPushRval(numbers, (ui8)atoi(numReader));
            stackArrayClear(numReader);

            if ( n > 0 ) {
                safe = safe && isSafe(numbers[ n ], numbers[ n - 1 ], &dir);
            }
            n++;
        }

        if ( c == '\n' || c == EOF ) {
            if ( safe ) {
                results[ Part1 ]++;
                results[ Part2 ]++;
            } else {
                for ( i8 i = 0; i < stackArrayGetHeader(numbers)->length; i++ ) {
                    dir           = NAN;
                    safe          = true;
                    ui8* dampened = stackArraySplice(numbers, i, 1);
                    for ( i8 j = 1; safe && j < stackArrayGetHeader(dampened)->length; j++ ) {
                        safe = isSafe(dampened[ j ], dampened[ j - 1 ], &dir);
                    }
                    stackArrayFree(dampened);
                    if ( safe ) {
                        results[ Part2 ]++;
                        break;
                    }
                }
            }

            stackArrayClear(numbers);
            n    = 0;
            dir  = NAN;
            safe = true;

            if ( c == EOF )
                break;
        }
    }

    stackArrayFree(numbers);
    stackArrayFree(numReader);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 2, &algorithm);
    return EXIT_SUCCESS;
}
