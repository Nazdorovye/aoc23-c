#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define REGION_INIT_COUNT 100

typedef struct _Region {
    i64 perimeter;
    i32 area;
    i32 corners;
} Region;

typedef struct _Plot {
    i16 regionId;
    i8 symbol;
    union {
        struct {
            bool top : 1;
            bool right : 1;
            bool bottom : 1;
            bool left : 1;
        } split;
        ui8 data;
    } borders;
} Plot;

typedef union _Delta {
    struct {
        i8 x : 4, y : 4;
    } split;
    ui8 data;
} Delta;

static Region* regions;
static Plot** map;
static ui64 colCount, rowCount;

void crawl(i64 row, i64 col, Plot* prev, Delta delta) {
    i64 newRow = row + delta.split.y;
    i64 newCol = col + delta.split.x;

    if ( newRow < 0 || newCol < 0 || newRow >= rowCount || newCol >= colCount ) {
        return;
    }

    Plot* curr = &map[ newRow ][ newCol ];

    if ( curr->symbol != prev->symbol ) {
        return;
    }

    bool wasNotVisited = curr->regionId == -1;
    if ( wasNotVisited ) {
        curr->regionId = prev->regionId;
        regions[ prev->regionId ].area++;
        regions[ prev->regionId ].perimeter += 4;
    }

    if ( delta.split.y == -1 ) {
        regions[ prev->regionId ].perimeter -= (curr->borders.split.bottom + prev->borders.split.top);
        curr->borders.split.bottom = false;
        prev->borders.split.top    = false;
    }

    else if ( delta.split.y == 1 ) {
        regions[ prev->regionId ].perimeter -= (curr->borders.split.top + prev->borders.split.bottom);
        curr->borders.split.top    = false;
        prev->borders.split.bottom = false;
    }

    else if ( delta.split.x == -1 ) {
        regions[ prev->regionId ].perimeter -= (curr->borders.split.right + prev->borders.split.left);
        curr->borders.split.right = false;
        prev->borders.split.left  = false;
    }

    else if ( delta.split.x == 1 ) {
        regions[ prev->regionId ].perimeter -= (curr->borders.split.left + prev->borders.split.right);
        curr->borders.split.left  = false;
        prev->borders.split.right = false;
    }

    regions[ prev->regionId ].perimeter < 0 && (regions[ prev->regionId ].perimeter = 0);

    if ( !wasNotVisited ) {
        return;
    }

    crawl(newRow, newCol, curr, (Delta){.split.x = 1, .split.y = 0});
    crawl(newRow, newCol, curr, (Delta){.split.x = -1, .split.y = 0});
    crawl(newRow, newCol, curr, (Delta){.split.x = 0, .split.y = 1});
    crawl(newRow, newCol, curr, (Delta){.split.x = 0, .split.y = -1});
}

bool checkConcaveCorner(i64 row, i64 col, Plot* prev, Delta delta) {
    i64 newRow = row + delta.split.y;
    i64 newCol = col + delta.split.x;
    if ( newRow < 0 || newCol < 0 || newRow >= rowCount || newCol >= colCount ) {
        return false;
    }

    Plot* curr = &map[ newRow ][ newCol ];
    if ( curr->regionId != prev->regionId ) {
        return false;
    }

    if ( delta.split.y == -1 ) {
        if ( delta.split.x == -1 ) {
            return curr->borders.split.bottom && prev->borders.split.left;
        } else if ( delta.split.x == 1 ) {
            return curr->borders.split.bottom && prev->borders.split.right;
        }
    } else if ( delta.split.y == 1 ) {
        if ( delta.split.x == -1 ) {
            return curr->borders.split.top && prev->borders.split.left;
        } else if ( delta.split.x == 1 ) {
            return curr->borders.split.top && prev->borders.split.right;
        }
    }

    return false;
}

void algorithm(FILE* file, ui64* results) {
    colCount = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file), colCount++ ) {}

    fseek(file, 0, SEEK_END);
    rowCount = ftell(file) / colCount;
    rewind(file);

    regions = stackArrayInit(REGION_INIT_COUNT, sizeof(Region), NULL);

    map = calloc(rowCount, sizeof(Plot*));
    for ( ui64 row = 0; row < rowCount; row++ ) {
        map[ row ] = calloc(colCount, sizeof(Plot));
        for ( ui64 col = 0; col < colCount; col++ ) {
            i8 c              = fgetc(file);
            map[ row ][ col ] = (Plot){.regionId = -1, .symbol = c, .borders.data = UINT8_MAX};
        }
        fgetc(file); // skip newline
    }

    for ( ui64 row = 0; row < rowCount; row++ ) {
        for ( ui64 col = 0; col < colCount; col++ ) {
            Plot* plot = &map[ row ][ col ];
            if ( plot->regionId != -1 ) {
                continue;
            }

            plot->regionId = stackArrayGetHeader(regions)->length;
            stackArrayPushRval(regions, ((Region){.area = 1, .perimeter = 4}));

            crawl(row, col, plot, (Delta){.split.x = 1, .split.y = 0});
            crawl(row, col, plot, (Delta){.split.x = -1, .split.y = 0});
            crawl(row, col, plot, (Delta){.split.x = 0, .split.y = 1});
            crawl(row, col, plot, (Delta){.split.x = 0, .split.y = -1});
        }
    }

    for ( ui64 i = 0; i < stackArrayGetHeader(regions)->length; i++ ) {
        results[ Part1 ] += regions[ i ].area * regions[ i ].perimeter;
    }

    for ( ui64 row = 0; row < rowCount; row++ ) {
        for ( ui64 col = 0; col < colCount; col++ ) {
            Plot* plot     = &map[ row ][ col ];
            Region* region = &regions[ plot->regionId ];

            // corners
            bool topRight    = plot->borders.split.top * plot->borders.split.right;
            bool topLeft     = plot->borders.split.top * plot->borders.split.left;
            bool bottomRight = plot->borders.split.bottom * plot->borders.split.right;
            bool bottomLeft  = plot->borders.split.bottom * plot->borders.split.left;

            // convex
            region->corners += topRight + topLeft + bottomRight + bottomLeft;

            // concave
            if ( !topRight ) {
                region->corners += checkConcaveCorner(row, col, plot, (Delta){.split.x = 1, .split.y = -1});
            }
            if ( !topLeft ) {
                region->corners += checkConcaveCorner(row, col, plot, (Delta){.split.x = -1, .split.y = -1});
            }
            if ( !bottomRight ) {
                region->corners += checkConcaveCorner(row, col, plot, (Delta){.split.x = 1, .split.y = 1});
            }
            if ( !bottomLeft ) {
                region->corners += checkConcaveCorner(row, col, plot, (Delta){.split.x = -1, .split.y = 1});
            }
        }
    }

    for ( ui64 i = 0; i < stackArrayGetHeader(regions)->length; i++ ) {
        results[ Part2 ] += regions[ i ].area * regions[ i ].corners;
    }

    for ( ui64 row = 0; row < rowCount; row++ ) {
        free(map[ row ]);
    }
    free(map);
    stackArrayFree(regions);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 12, &algorithm);
    return EXIT_SUCCESS;
}
