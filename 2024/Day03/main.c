#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

#define NUM_CAP  4
#define isNum(x) x >= '0' && x <= '9'

typedef enum _Expected {
    none,
    d    = 'd',
    o    = 'o',
    n    = 'n',
    ap   = '\'',
    m    = 'm',
    t    = 't',
    u    = 'u',
    l    = 'l',
    br_o = '(',
    br_c = ')',
    sep  = ',',
    num
} Expected;

void algorithm(FILE* file, ui64* results) {
    Expected exp[ 2 ] = {d, m};
    bool isDo         = false;
    bool isDont       = false;
    bool _do          = true;

    i8* numReader    = stackArrayInit(NUM_CAP, sizeof(i8), NULL);
    i16 numbers[ 2 ] = {0};
    i8 numIdx        = 0;

    for ( i8 c = fgetc(file);; c = fgetc(file) ) {
        if ( exp[ 0 ] == num ) {
            if ( numIdx == 0 && c == sep ) {
                numbers[ numIdx ] = atoi(numReader);
                stackArrayClear(numReader);
                numIdx = 1;
                continue;
            }
            if ( isNum(c) ) {
                stackArrayPushRval(numReader, c);
                continue;
            }
            if ( numIdx == 1 && c == br_c ) {
                numbers[ numIdx ] = atoi(numReader);
                stackArrayClear(numReader);
                i64 mult = numbers[ 0 ] * numbers[ 1 ];
                results[ Part1 ] += mult;
                results[ Part2 ] += mult * _do;
                if ( _do ) {
                    printf("%d %d\n", numbers[ 0 ], numbers[ 1 ]);
                }
                numIdx = 0;
            }
            goto cycleEnd;
        }
        numIdx = 0;
        stackArrayClear(numReader);

        bool _continue = false;
        for ( i8 i = 0; i < 2; i++ ) {
            if ( exp[ i ] == none )
                break;
            if ( exp[ i ] == c ) {
                _continue = true;
                switch ( exp[ i ] ) {
                    case d:
                        isDo     = true;
                        exp[ 0 ] = o;
                        exp[ 1 ] = none;
                        break;
                    case o:
                        exp[ 0 ] = br_o;
                        exp[ 1 ] = n;
                        break;
                    case n:
                        isDo     = false;
                        isDont   = true;
                        exp[ 0 ] = ap;
                        exp[ 1 ] = none;
                        break;
                    case ap:
                        exp[ 0 ] = t;
                        exp[ 1 ] = none;
                        break;
                    case t:
                        exp[ 0 ] = br_o;
                        exp[ 1 ] = none;
                        break;

                    case m:
                        isDo     = false;
                        isDont   = false;
                        exp[ 0 ] = u;
                        exp[ 1 ] = none;
                        break;
                    case u:
                        exp[ 0 ] = l;
                        exp[ 1 ] = none;
                        break;
                    case l:
                        exp[ 0 ] = br_o;
                        exp[ 1 ] = none;
                        break;

                    case br_o:
                        exp[ 0 ] = isDo || isDont ? br_c : num;
                        exp[ 1 ] = none;
                        break;

                    case br_c:
                        if ( isDo )
                            _do = true;
                        if ( isDont )
                            _do = false;
                        goto cycleEnd;
                }
            }
        }
        if ( _continue )
            continue;

        if ( c == EOF )
            break;

    cycleEnd:
        exp[ 0 ] = m;
        exp[ 1 ] = d;
    }

    stackArrayFree(numReader);
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 3, &algorithm);
    return EXIT_SUCCESS;
}
