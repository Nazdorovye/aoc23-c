#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define NUM_LENGTH 5
#define ROW_COUNT  1000

void algorithm(FILE* file, ui64* results) {
    i8StrReader* numReader = i8StrReader_initialize(NUM_LENGTH);

    ui32 data[ 2 ][ ROW_COUNT ] = {0, 0};
    ui32 rowIndex               = 0;
    i8 colIndex                 = 0;

    for ( i8 prev_c, c = fgetc(file);; prev_c = c, c = getc(file) ) {
        if ( c == ' ' || c == '\n' || c == EOF ) {
            if ( prev_c != ' ' ) {
                ui32 currentVal = atoi(numReader->buffer);

                // sort in place
                ui32 whereToPlace = rowIndex;
                for ( i32 i = 0; i < whereToPlace; i++ ) {
                    if ( currentVal <= data[ colIndex ][ i ] ) {
                        memmove(&data[ colIndex ][ i + 1 ], &data[ colIndex ][ i ], sizeof(ui32) * (rowIndex - i));
                        whereToPlace = i;
                        break;
                    }
                }

                data[ colIndex ][ whereToPlace ] = currentVal;
                i8StrReader_clear(numReader);
                colIndex ^= 1;
            }
            if ( c == '\n' )
                rowIndex++;
            if ( c == EOF )
                break;
            continue;
        }
        i8StrReader_putChar(numReader, c);
    }

    i8StrReader_free(numReader);

    for ( ui32 row = 0; row < ROW_COUNT; row++ ) {
        results[ Part1 ] += max(data[ 0 ][ row ], data[ 1 ][ row ]) - min(data[ 0 ][ row ], data[ 1 ][ row ]);

        ui32 simScore = 0;
        for ( ui32 i = 0; i < ROW_COUNT; i++ ) {
            if ( data[ 0 ][ row ] == data[ 1 ][ i ] )
                simScore++;
        }

        results[ Part2 ] += data[ 0 ][ row ] * simScore;
    }
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 1, &algorithm);
    return EXIT_SUCCESS;
}
