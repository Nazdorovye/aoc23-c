#ifndef _STR_READER_H
#define _STR_READER_H

#include "types.h"
#include <stdlib.h>
#include <string.h>

#define SR_OK       0
#define SR_NULL     1
#define SR_NOT_NULL 2

#define structStringReader(name, charType)                       \
    struct _##name {                                             \
        charType* buffer;                                        \
        ui64 charIdx;                                            \
    } name;                                                      \
    ui32 name##_clear(name* stringReader);                       \
    name* name##_initialize(ui32 bufferLength);                  \
    ui32 name##_popString(name* stringReader, charType* target); \
    void name##_putChar(name* stringReader, charType character); \
    void name##_free(name* stringReader);

typedef structStringReader(i8StrReader, i8);
typedef structStringReader(ui8StrReader, ui8);
typedef structStringReader(ui16StrReader, ui16);
typedef structStringReader(ui32StrReader, ui32);

#endif // _STR_READER_H
