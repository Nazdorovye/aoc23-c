#ifndef _AOC_MATH_H
#define _AOC_MATH_H

#ifdef AOC_MATH_SQRT
    #include "quake-sqrt.h"
#endif // AOC_MATH_SQRT

#ifdef AOC_MATH_QUADRATIC
    #include "quadratic.h"
#endif // AOC_MATH_QUADRATIC

#ifdef AOC_MATH_GCD
    #include "gcd.h"
#endif // AOC_MATH_GCD

#ifdef AOC_MATH_LCM
    #include "lcm.h"
#endif // AOC_MATH_LCM

#define ctoi8(c) (ui8)(c - '0')

#define max(a, b)               \
    ({                          \
        __typeof__(a) _a = (a); \
        __typeof__(b) _b = (b); \
        _a > _b ? _a : _b;      \
    })

#define min(a, b)               \
    ({                          \
        __typeof__(a) _a = (a); \
        __typeof__(b) _b = (b); \
        _a < _b ? _a : _b;      \
    })

#define digitCount(number)                                   \
    ({                                                       \
        ui8 count = 0;                                       \
        for ( ui64 n = number; n != 0; n /= 10, count++ ) {} \
        count;                                               \
    })

#endif // _AOC_MATH_H
