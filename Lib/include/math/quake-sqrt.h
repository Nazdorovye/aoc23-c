#ifndef _QUAKE_SQRT_H
#define _QUAKE_SQRT_H

#include "../types.h"

f32 quake_inverse_sqrt_f32(f32 n);
f32 quake_sqrt_f32(f32 n);

#endif // _QUAKE_SQRT_H