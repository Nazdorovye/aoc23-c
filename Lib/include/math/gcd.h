#ifndef _GCD_H
#define _GCD_H

#include "../types.h"
#include <stdarg.h>

i64 gcd(i64 a, i64 b);
i64 gcds(ui32 count, ...);
i64 gcds_a(ui32 count, i64* array);

i64 va_gcds(ui32 count, va_list vaList);

#endif // _GCD_H
