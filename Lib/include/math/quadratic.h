#ifndef _QUADRATIC_H
#define _QUADRATIC_H

#include "../types.h"

#define structQuadEq(name, iT, fT)\
    struct _##name##_##fT {\
        fT x1;\
        fT x2;\
        ui8 resultCount;\
    } name##_##fT;\
    name##_##fT calcQuadEq_##iT(iT a, iT b, iT c);

typedef structQuadEq(QuadEqResult, i32, f32)
typedef structQuadEq(QuadEqResult, i64, f64)

#endif // _QUADRATIC_H
