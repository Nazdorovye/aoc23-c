#ifndef _LCM_H
#define _LCM_H

#include "../types.h"

i64 lcm(i64 a, i64 b);
i64 lcms(ui32 count, ...);
i64 lcms_a(ui32 count, i64* array);

#endif
