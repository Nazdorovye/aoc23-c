#ifndef _STACK_ARRAY_C
#define _STACK_ARRAY_C

#include "../types.h"
#include <stddef.h>

typedef void (*SADestructor)(void* data);

typedef struct _SAHeader {
    ui64 signature;
    ui64 capacity;
    ui64 length;
    ui64 elementSize;
    SADestructor destructor;
} SAHeader;

extern void* stackArrayInit(ui64 initialCapacity, ui64 elementSize, SADestructor destructor);
extern void stackArrayFree(void* stackArray);

extern SAHeader* stackArrayGetHeader(void* stackArray);
extern void* stackArrayPush(void** stackArray, void* data, ui64 dataSize);
extern bool stackArrayPop(void* stackArray, void* dest);
extern void stackArrayClear(void* stackArray);
extern void* stackArraySplice(void* stackArray, ui64 start, ui64 count);

#define stackArrayPushRval(stackArray, data)                       \
    ({                                                             \
        __auto_type pData = data;                                  \
        stackArrayPush((void*)&stackArray, &pData, sizeof(pData)); \
    })

#endif // _STACK_ARRAY_C
