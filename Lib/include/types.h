#ifndef _TYPES_H
#define _TYPES_H

#include <stdbool.h>

typedef char i8;
typedef short i16;
typedef int i32;
typedef long i64;

typedef signed char si8;
typedef signed short si16;
typedef signed int si32;
typedef signed long si64;

typedef unsigned char ui8;
typedef unsigned short ui16;
typedef unsigned int ui32;
typedef unsigned long ui64;

typedef float f32;
typedef double f64;

#endif // _TYPES_H
