#ifndef _AOC_LIB_H
#define _AOC_LIB_H

#include "types.h"
#include <stdio.h>

#ifdef AOC_MATH
    #include "math/aoc-math.h"
#endif // AOC_MATH

#ifdef AOC_STR_READER
    #include "str-reader.h"
#endif // AOC_STR_READER

#ifdef AOC_STACK_ARRAY
    #include "memory/stack-array.h"
#endif // AOC_STACK_ARRAY

#define MEDIAN_CYCLES 1
#define Part1         0
#define Part2         1

#define isDigit(c)            c >= '0' && c <= '9'
#define isCapLetter(c)        c >= 'A' && c <= 'Z'
#define isLetter(c)           c >= 'a' && c <= 'z'
#define funPtr(name, args...) (*name)(args)

void openAndRun(ui32 argc, i8* argv[], ui8 day, void funPtr(callback, FILE* pFile, ui64* results));

#endif // _AOC_LIB_H
