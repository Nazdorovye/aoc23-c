#include "../../include/memory/stack-array.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define arrayHeadSignature 0x414853 /* = AHS */
#define headerFieldSize    sizeof(size_t)
#define resizeMultiplier   2

#define _headerSize                     sizeof(SAHeader)
#define _getStartPointer(pSAHeader)     (void*)pSAHeader + _headerSize
#define _at(pArray, index, elementSize) (pArray + (elementSize * index))

#ifdef __GNUC__
    #define likely(x)   __builtin_expect(!!(x), 1)
    #define unlikely(x) __builtin_expect(!!(x), 0)
#else
    #define likely(x)   x
    #define unlikely(x) x
#endif

#define fileNamePrefix   "[" __FILE_NAME__ "]:"
#define formattedLine(x) fileNamePrefix x "\n"

/**
 *  Allocates array and its header on the heap
 */
static SAHeader* _init(size_t initialCapacity, size_t elementSize, SADestructor destructor) {
    size_t arraySize = initialCapacity * elementSize;

    SAHeader* result = (SAHeader*)aligned_alloc(headerFieldSize, _headerSize + arraySize);
    if ( unlikely(result == NULL) ) {
        perror(fileNamePrefix);
        printf(formattedLine("_init failed at aligned_alloc()"));
        return NULL;
    }
    memset(result, 0, _headerSize + arraySize);

    result->signature   = arrayHeadSignature;
    result->capacity    = initialCapacity;
    result->length      = 0;
    result->elementSize = elementSize;
    result->destructor  = destructor;

    return result;
}

void* stackArrayInit(size_t initialCapacity, size_t elementSize, SADestructor destructor) {
    return _getStartPointer(_init(initialCapacity, elementSize, destructor));
}

SAHeader* stackArrayGetHeader(void* stackArray) {
    SAHeader* header = (SAHeader*)(stackArray - _headerSize);

    if ( likely(header->signature == arrayHeadSignature) ) {
        return header;
    }

    printf(formattedLine("Attempted to access a header of non-managed array"));
    return NULL;
}

void stackArrayFree(void* stackArray) {
    if ( unlikely(stackArray == NULL) ) {
        printf(formattedLine("Attempted to free NULL"));
        return;
    }

    SAHeader* arrayHeader = stackArrayGetHeader(stackArray);
    if ( unlikely(arrayHeader == NULL) ) {
        printf(formattedLine("stackArrayFree failed at _header()"));
    }

    if ( arrayHeader->destructor != NULL ) {
        for ( size_t i = 0; i < arrayHeader->length; i++ ) {
            void** ptr = _at(stackArray, i, arrayHeader->elementSize);
            (arrayHeader->destructor)(*ptr);
        }
    }

    arrayHeader->signature = 0;
    free(arrayHeader);
    arrayHeader = NULL;
    stackArray  = NULL;
};

SAHeader* _resize(SAHeader* oldStackArrayHeader) {
    if ( oldStackArrayHeader->capacity == SIZE_MAX ) {
        printf(formattedLine("_resize failed, array capacity already reached its maximum"));
        return NULL;
    }

    size_t newCapacity = oldStackArrayHeader->capacity * resizeMultiplier;
    if ( unlikely(newCapacity < oldStackArrayHeader->capacity) ) {
        newCapacity = SIZE_MAX;
        printf(formattedLine("_resize overflown, new capacity is SIZE_MAX"));
    }

    SAHeader* newArrayHeader = _init(newCapacity, oldStackArrayHeader->elementSize, oldStackArrayHeader->destructor);
    if ( unlikely(newArrayHeader == NULL) ) {
        printf(formattedLine("_resize failed at _init()"));
        return NULL;
    }
    newArrayHeader->length = oldStackArrayHeader->length;

    memcpy(_getStartPointer(newArrayHeader), _getStartPointer(oldStackArrayHeader),
           oldStackArrayHeader->length * oldStackArrayHeader->elementSize);

    oldStackArrayHeader->signature = 0;
    free(oldStackArrayHeader);
    return newArrayHeader;
}

void* stackArrayPush(void** stackArray, void* data, size_t dataSize) {
    SAHeader* arrayHeader = stackArrayGetHeader(*stackArray);
    if ( unlikely(arrayHeader == NULL) ) {
        printf(formattedLine("brArrayPush failed at _header()"));
        return NULL;
    }

    if ( unlikely(dataSize > arrayHeader->elementSize) ) {
        printf(formattedLine("stackArrayPush failed at element size check: incoming data is larger than array element size"));
        return NULL;
    }

    if ( arrayHeader->length >= arrayHeader->capacity ) {
        SAHeader* newArrayHeader = _resize(arrayHeader);
        if ( unlikely(newArrayHeader == NULL) ) {
            printf(formattedLine("stackArrayPush failed at _resize()"));
            return NULL;
        }

        arrayHeader = newArrayHeader;
        *stackArray = _getStartPointer(newArrayHeader);
    }

    size_t offset = arrayHeader->elementSize * (arrayHeader->length)++;
    memcpy(*stackArray + offset, data, arrayHeader->elementSize);

    return *stackArray + offset;
}

bool stackArrayPop(void* stackArray, void* dest) {
    SAHeader* arrayHeader = stackArrayGetHeader(stackArray);
    if ( unlikely(arrayHeader == NULL) ) {
        printf(formattedLine("stackArrayPop failed at _header()"));
        return false;
    }

    if ( arrayHeader->length == 0 ) {
        return false;
    }

    size_t offset = arrayHeader->elementSize * --(arrayHeader->length);
    if ( dest != NULL ) {
        memcpy(dest, stackArray + offset, arrayHeader->elementSize);
        return true;
    }
    return false;
}

void stackArrayClear(void* stackArray) {
    SAHeader* arrayHeader = stackArrayGetHeader(stackArray);
    if ( unlikely(arrayHeader == NULL) ) {
        printf(formattedLine("stackArrayClear failed at _header()"));
        return;
    }

    memset(stackArray, 0, arrayHeader->length * arrayHeader->elementSize);
    arrayHeader->length = 0;
}

extern void* stackArraySplice(void* stackArray, ui64 start, ui64 count) {
    SAHeader* arrayHeader = stackArrayGetHeader(stackArray);
    if ( unlikely(arrayHeader == NULL) ) {
        printf(formattedLine("stackArraySplice failed at _header()"));
        return NULL;
    }

    if ( unlikely(start + count > arrayHeader->capacity) ) {
        printf(formattedLine("stackArraySplice failed at capacity check: out of bounds"));
        return NULL;
    }

    ui64 length        = arrayHeader->length - count;
    SAHeader* newArray = _init(length, arrayHeader->elementSize, arrayHeader->destructor);
    newArray->length   = length;

    void* result = _getStartPointer(newArray);

    memmove(result, stackArray, arrayHeader->elementSize * start);
    memmove(_at(result, start, arrayHeader->elementSize), _at(stackArray, start + 1, arrayHeader->elementSize),
            arrayHeader->elementSize * (length - start + 1));

    return result;
}
