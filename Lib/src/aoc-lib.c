#define _XOPEN_SOURCE 700

#include "../include/aoc-lib.h"

#include <stdlib.h>
#include <string.h>
#include <time.h>

void openAndRun(ui32 argc, i8* argv[], ui8 day, void funPtr(callback, FILE* pFile, ui64* results)) {
    if (argc != 2) {
        printf("Usage: %%command%% [input txt]\n\n");
        exit(EXIT_FAILURE);
    }

    FILE* file = fopen(argv[1], "r");
    if (!file) {
        perror("Error opening file");
        printf("\n");
        exit(EXIT_FAILURE);
    }

    f64 medianNs = 0;
    ui64 results[2] = { 0, 0 };

    for (unsigned i = 0; i < MEDIAN_CYCLES; i++) {
        struct timespec dtStart, dtEnd;
        clock_gettime(CLOCK_MONOTONIC_RAW, &dtStart);

        ui64 r[2] = { 0, 0 };
        callback(file, r);

        clock_gettime(CLOCK_MONOTONIC_RAW, &dtEnd);
        medianNs = (medianNs + (f64)(dtEnd.tv_nsec - dtStart.tv_nsec)) / 2.f;

        rewind(file);
        memcpy(results, r, 2 * sizeof(ui64));
    }
    printf("\n");
    for (ui8 part = 0; part <= Part2; part++) printf("Day %u Part %u answer: %lu\n", day, part + 1, results[part]);
    printf("Median execution time for %d cycles:\n\t %f ms\n\n", MEDIAN_CYCLES, medianNs / 1000000.f);

    fclose(file);
};
