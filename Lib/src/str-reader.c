#include "../include/str-reader.h"

#define funcDecl(name, charType)                                                        \
    ui32 name##_clear(name* stringReader) {                                             \
        if ( !stringReader )                                                            \
            return SR_NULL;                                                             \
        memset(stringReader->buffer, 0, sizeof(charType) * stringReader->charIdx);      \
        stringReader->charIdx = 0;                                                      \
        return SR_OK;                                                                   \
    };                                                                                  \
    name* name##_initialize(ui32 bufferLength) {                                        \
        name* temp   = (name*)malloc(sizeof(name));                                     \
        temp->buffer = (charType*)malloc(sizeof(charType) * bufferLength);              \
        name##_clear(temp);                                                             \
        return temp;                                                                    \
    };                                                                                  \
    ui32 name##_popString(name* stringReader, charType* target) {                       \
        if ( !stringReader )                                                            \
            return SR_NULL;                                                             \
        memcpy(target, stringReader->buffer, sizeof(charType) * stringReader->charIdx); \
        return name##_clear(stringReader);                                              \
    };                                                                                  \
    void name##_putChar(name* stringReader, charType character) {                       \
        stringReader->buffer[ stringReader->charIdx++ ] = character;                    \
    };                                                                                  \
    void name##_free(name* stringReader) {                                              \
        free(stringReader->buffer);                                                     \
        free(stringReader);                                                             \
    }

funcDecl(i8StrReader, i8);
funcDecl(ui8StrReader, ui8);
funcDecl(ui16StrReader, ui16);
funcDecl(ui32StrReader, ui32);
