#include "../../include/math/lcm.h"

#include <stdarg.h>

#define AOC_MATH_GCD
#include "../../include/math/gcd.h"

i64 lcm(i64 a, i64 b) {
    return (a * b) / gcd(a, b);
}

i64 lcms(ui32 count, ...) {
    va_list vaList;
    i64 result, _gcds;

    va_start(vaList, count);
    {
        _gcds = va_gcds(count, vaList);
    }
    va_end(vaList);

    va_start(vaList, count);
    {
        result = va_arg(vaList, i64);
        for (ui32 i = 1; i < count; i++) result *= va_arg(vaList, i64);
    }
    va_end(vaList);

    return result / _gcds;
}

i64 lcms_a(ui32 count, i64* array) {
    i64 result = lcm(array[0], array[1]);
    for (ui32 i = 2; i < count; i++) result = lcm(result, array[i]);
    return result;
}
