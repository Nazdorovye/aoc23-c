#include "../../include/math/gcd.h"

#include <stdlib.h>

i64 gcd(i64 a, i64 b) {
    while (b != 0) {
        i64 t = b;
        b = a % b;
        a = t;
    }
    return abs(a);
}

i64 va_gcds(ui32 count, va_list vaList) {
    i64 result = gcd(va_arg(vaList, i64), va_arg(vaList, i64));
    for (ui32 i = 2; i < count; i++) result = gcd(result, va_arg(vaList, i64));
    return result;
}

i64 gcds(ui32 count, ...) {
    va_list vaList;
    i64 result;

    va_start(vaList, count);
    {
        result = va_gcds(count, vaList);
    }
    va_end(vaList);

    return result;
}

i64 gcds_a(ui32 count, i64* array) {
    i64 result = gcd(array[0], array[1]);
    for (ui32 i = 2; i < count; i++) result = gcd(result, array[i]);    
    return result;
}
