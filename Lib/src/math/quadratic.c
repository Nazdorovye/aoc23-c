#include "../../include/math/quadratic.h"

#include <math.h>

#define AOC_MATH_SQRT
#include "../../include/math/quake-sqrt.h"

#define quake_sqrt_f64 sqrt

#define funcStd(name, iT, fT)\
    static inline QuadEqResult_##fT name##_##iT(iT a, iT b, iT c) {\
        QuadEqResult_##fT result = { 0.f, 0.f, 0 };\
        iT D = (b * b) - (4 * a * c);\
        iT _2a = 2 * a;\
        if (D) {\
            fT _sqrtD = quake_sqrt_##fT(D);\
            result.x1 = (-b + _sqrtD) / _2a;\
            result.x2 = (-b - _sqrtD) / _2a;\
            result.resultCount = 2;\
        } if (D == 0) {\
            result.x1 = -((fT)b / (fT)_2a);\
            result.resultCount = 1;\
        }\
        return result;\
    }

funcStd(calcQuadEqStd, i32, f32)
funcStd(calcQuadEqStd, i64, f64)

#define funcReduced(name, iT, fT)\
    static inline QuadEqResult_##fT name##_##iT(iT a, iT b, iT c) {\
        QuadEqResult_##fT result = { 0.f, 0.f, 0 };\
        iT k = b / 2;\
        iT k2 = k * k;\
        iT y = k2 - c;\
        iT D = 4 * y;\
        if (D) {\
            fT _sqrt = quake_sqrt_##fT(y);\
            result.x1 = -k + _sqrt;\
            result.x2 = -k - _sqrt;\
            result.resultCount = 2;\
        } else if (D == 0) {\
            result.x1 = -k;\
            result.resultCount = 1;\
        }\
        return result;\
    }

funcReduced(calcQuadEqRed, i32, f32)
funcReduced(calcQuadEqRed, i64, f64)

#define funcQuadEq(name, iT, fT)\
    QuadEqResult_##fT name##_##iT(iT a, iT b, iT c) {\
        if (b % 2) {\
            return calcQuadEqStd_##iT(a, b, c);\
        }\
        return calcQuadEqRed_##iT(a, b, c);\
    }

funcQuadEq(calcQuadEq, i32, f32)
funcQuadEq(calcQuadEq, i64, f64)

#undef quake_sqrt_f64
