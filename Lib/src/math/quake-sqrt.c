#include "../../include/math/quake-sqrt.h"

/** == https://thatonegamedev.com/math/fast-square-root-quake-iii/#my-c-implementation == **/
f32 quake_inverse_sqrt_f32(f32 n) {
    union _data {
        f32 x_0;
        i32 integerRepresentation;
    } data;
    
    data.x_0 = n;
    const i32 K_correction = 0x5f3759df;
    data.integerRepresentation = K_correction - (data.integerRepresentation >> 1);
    return data.x_0 * (1.5f - (n * 0.5) * (data.x_0 * data.x_0));
}

f32 quake_sqrt_f32(f32 n) {
    return quake_inverse_sqrt_f32(n) * n;
}
