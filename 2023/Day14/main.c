#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define PART2_CYCLES 1000000000

typedef enum _TiltDirection { N, W, S, E } TiltDirection;

static ui8** m;
static ui32 cols, rows;

#define INITIAL_CACHE_SIZE 1024
static ui64* cache;
static i64 cacheNext;
static ui64 cacheSize;

static inline void tilt(TiltDirection dir) {
    switch ( dir ) {
        case N:
            for ( i8 row = 0; row < rows; row++ ) {
                for ( i8 col = 0; col < cols; col++ ) {
                    if ( m[ row ][ col ] != 'O' )
                        continue;
                    ui32 _row = row;
                    for ( ; _row > 0; _row-- )
                        if ( m[ _row - 1 ][ col ] != '.' )
                            break;
                    m[ _row ][ col ] = 'O';
                    if ( row != _row )
                        m[ row ][ col ] = '.';
                }
            }
            break;

        case W:
            for ( i8 col = 0; col < cols; col++ ) {
                for ( i8 row = 0; row < rows; row++ ) {
                    if ( m[ row ][ col ] != 'O' )
                        continue;
                    ui32 _col = col;
                    for ( ; _col > 0; _col-- )
                        if ( m[ row ][ _col - 1 ] != '.' )
                            break;
                    m[ row ][ _col ] = 'O';
                    if ( col != _col )
                        m[ row ][ col ] = '.';
                }
            }
            break;

        case S:
            for ( i8 row = rows - 1; row >= 0; row-- ) {
                for ( i8 col = 0; col < cols; col++ ) {
                    if ( m[ row ][ col ] != 'O' )
                        continue;
                    ui32 _row = row;
                    for ( ; _row < rows - 1; _row++ )
                        if ( m[ _row + 1 ][ col ] != '.' )
                            break;
                    m[ _row ][ col ] = 'O';
                    if ( row != _row )
                        m[ row ][ col ] = '.';
                }
            }
            break;

        case E:
            for ( i8 col = cols - 1; col >= 0; col-- ) {
                for ( i8 row = 0; row < rows; row++ ) {
                    if ( m[ row ][ col ] != 'O' )
                        continue;
                    ui32 _col = col;
                    for ( ; _col < cols - 1; _col++ )
                        if ( m[ row ][ _col + 1 ] != '.' )
                            break;
                    m[ row ][ _col ] = 'O';
                    if ( col != _col )
                        m[ row ][ col ] = '.';
                }
            }
    }
}

static inline ui64 getMatrixHash() {
    ui64 hash = 17;
    for ( i8 row = 0; row < rows; row++ ) {
        for ( i8 col = 0; col < cols; col++ ) {
            hash = hash * 31 + m[ row ][ col ];
        }
    }
    return hash;
}

static inline void expandCache() {
    if ( cacheNext < cacheSize )
        return;
    cacheSize += INITIAL_CACHE_SIZE;
    cache = realloc(cache, cacheSize);
    memset(&cache[ cacheNext ], 0, sizeof(ui64) * (cacheSize - cacheNext));
}

static inline void pushCache(ui64 cacheValue) {
    cache[ cacheNext++ ] = cacheValue;
    if ( cacheNext >= cacheSize )
        expandCache();
}

void algorithm(FILE* file, ui64* results) {
    cols = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file) )
        cols++;
    fseek(file, 0, SEEK_END);
    rows = (ftell(file) + 1) / (cols + 1);
    rewind(file);

    m = malloc(sizeof(ui8*) * rows);
    for ( ui8 r = 0; r < rows; r++ ) {
        m[ r ] = malloc(sizeof(ui8) * rows);
        memset(m[ r ], 0, sizeof(ui8) * rows);
    }

    ui32 row = 0;
    ui32 col = 0;
    for ( i8 c = fgetc(file); c != EOF; c = fgetc(file) ) {
        if ( c == '\n' ) {
            row++;
            col = 0;
            continue;
        }

        if ( c == 'O' ) {
            ui32 _row = row;
            for ( ; _row > 0; _row-- )
                if ( m[ _row - 1 ][ col ] != '.' )
                    break;
            m[ _row ][ col ] = 'O';
            if ( row != _row )
                m[ row ][ col ] = '.';
            results[ Part1 ] += rows - _row;
        } else {
            m[ row ][ col ] = c;
        }

        col++;
    }

    cacheSize = INITIAL_CACHE_SIZE;
    cacheNext = 0;
    cache     = malloc(sizeof(ui64) * cacheSize);
    memset(cache, 0, cacheSize);

    for ( ui64 cycle = 0, skip = 0; cycle < PART2_CYCLES; cycle++ ) {
        for ( TiltDirection dir = N; dir <= E; dir++ )
            tilt(dir);

        if ( skip )
            continue;

        ui64 cycleHash = getMatrixHash();
        for ( ui64 ca = 0; ca < cacheNext; ca++ )
            if ( cache[ ca ] == cycleHash ) {
                ui64 cycleLength        = cycle - ca;
                ui64 cycleCountRequired = (PART2_CYCLES - cycle) / cycleLength;
                ui64 cyclesLeft         = PART2_CYCLES - cycle - cycleCountRequired * cycleLength;
                cycle = skip = PART2_CYCLES - cyclesLeft;
                break;
            }
        pushCache(cycleHash);
    }

    for ( ui32 row = 0; row < rows; row++ )
        for ( ui32 col = 0; col < cols; col++ )
            if ( m[ row ][ col ] == 'O' )
                results[ Part2 ] += rows - row;

    free(cache);
    for ( ui8 r = 0; r < rows; r++ )
        free(m[ r ]);
    free(m);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 14, &algorithm);
    return EXIT_SUCCESS;
}
