#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define SIZE 16384

typedef struct _QueueItem {
    ui32 heatLoss;
    ui8 x, y;
    ui8 steps;
    i8 dirX : 3;
    i8 dirY : 3;
} QueueItem;

typedef struct _Tile {
    ui16 l;
    ui16 r;
    ui16 u;
    ui16 d;
    ui8 value;
} Tile;

static ui8 cols, rows;
static Tile** grid;

static QueueItem pop(QueueItem* queue, ui64* queueIdx) {
    return queue[ --(*queueIdx) ];
}

static void push(QueueItem* queue, ui64* queueIdx, QueueItem item) {
    if ( queue[ (*queueIdx) - 1 ].heatLoss >= item.heatLoss ) {
        queue[ (*queueIdx)++ ] = item;
        return;
    }

    if ( queue[ 0 ].heatLoss <= item.heatLoss ) {
        memmove(&queue[ 1 ], &queue[ 0 ], (*queueIdx)++ * sizeof(QueueItem));
        queue[ 0 ] = item;
        return;
    }

    i64 mid = 0;
    for ( i64 l = 1, h = (*queueIdx) - 2; l <= h; ) {
        mid = l + (h - l) / 2;
        if ( queue[ mid ].heatLoss == item.heatLoss )
            break;

        if ( queue[ mid ].heatLoss >= item.heatLoss )
            l = mid + 1;
        else
            h = mid - 1;
    }

    memmove(&queue[ mid + 1 ], &queue[ mid ], ((*queueIdx)++ - mid) * sizeof(QueueItem));
    queue[ mid ] = item;
}

static inline ui16 isNthBitSet(ui16* c, ui16 n) {
    return *c & ((ui16)1 << n);
}

static inline void setNthBit(ui16* c, ui16 n, ui16 set) {
    *c = (*c & ~((ui16)1 << n)) | ((ui16)set << n);
}

static ui8 inSeen(Tile** grid, QueueItem* queueItem, ui8 part) {
    ui8 offset = part ? 3 : 0;
    return queueItem->dirX < 0 && isNthBitSet(&grid[ queueItem->y ][ queueItem->x ].l, queueItem->steps + offset) ||
           queueItem->dirX > 0 && isNthBitSet(&grid[ queueItem->y ][ queueItem->x ].r, queueItem->steps + offset) ||
           queueItem->dirY < 0 && isNthBitSet(&grid[ queueItem->y ][ queueItem->x ].u, queueItem->steps + offset) ||
           queueItem->dirY > 0 && isNthBitSet(&grid[ queueItem->y ][ queueItem->x ].d, queueItem->steps + offset);
}

static void setSeen(Tile** grid, QueueItem* queueItem, ui8 part) {
    ui8 offset = part ? 3 : 0;
    if ( queueItem->dirX < 0 )
        setNthBit(&grid[ queueItem->y ][ queueItem->x ].l, queueItem->steps + offset, 1);
    else if ( queueItem->dirX > 0 )
        setNthBit(&grid[ queueItem->y ][ queueItem->x ].r, queueItem->steps + offset, 1);
    else if ( queueItem->dirY < 0 )
        setNthBit(&grid[ queueItem->y ][ queueItem->x ].u, queueItem->steps + offset, 1);
    else if ( queueItem->dirY > 0 )
        setNthBit(&grid[ queueItem->y ][ queueItem->x ].d, queueItem->steps + offset, 1);
}

static void moveOnward(QueueItem* current, QueueItem* queue, ui64* queueIdx, ui8 part) {
    ui8 maxSteps = part ? 10 : 3;

    if ( current->steps < maxSteps && (current->dirX || current->dirY) ) {
        i16 nx = current->x + current->dirX;
        i16 ny = current->y + current->dirY;
        if ( nx >= 0 && nx < cols && ny >= 0 && ny < rows ) {
            push(queue, queueIdx,
                 (QueueItem){.dirX     = current->dirX,
                             .dirY     = current->dirY,
                             .heatLoss = current->heatLoss + grid[ ny ][ nx ].value,
                             .steps    = current->steps + 1,
                             .x        = nx,
                             .y        = ny});
        }
    }
}

static void rotate(QueueItem* current, QueueItem* queue, ui64* queueIdx, ui8 part) {
    if ( part == Part2 && current->steps < 4 && (current->dirX || current->dirY) )
        return;

    if ( !abs(current->dirY) ) {
        for ( i8 dy = -1, dx = 0; dy <= 1; dy += 2 ) {
            i16 ny = current->y + dy;
            if ( ny >= 0 && ny < rows ) {
                push(queue, queueIdx,
                     (QueueItem){.dirX     = dx,
                                 .dirY     = dy,
                                 .heatLoss = current->heatLoss + grid[ ny ][ current->x ].value,
                                 .steps    = 1,
                                 .x        = current->x,
                                 .y        = ny});
            }
        }
    }

    if ( !abs(current->dirX) ) {
        for ( i8 dx = -1, dy = 0; dx <= 1; dx += 2 ) {
            i16 nx = current->x + dx;
            if ( nx >= 0 && nx < cols ) {
                push(queue, queueIdx,
                     (QueueItem){.dirX     = dx,
                                 .dirY     = dy,
                                 .heatLoss = current->heatLoss + grid[ current->y ][ nx ].value,
                                 .steps    = 1,
                                 .x        = nx,
                                 .y        = current->y});
            }
        }
    }
}

void algorithm(FILE* file, ui64* results) {
    cols = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file) )
        cols++;
    fseek(file, 0, SEEK_END);
    rows = (ftell(file) + 1) / (cols + 1);
    rewind(file);

    grid = malloc(sizeof(Tile*) * rows);
    for ( ui8 r = 0; r < rows; r++ ) {
        grid[ r ] = malloc(sizeof(Tile) * cols);
        memset(grid[ r ], 0, sizeof(Tile) * cols);
    }

    for ( i16 c = fgetc(file), row = 0; c != '\n' && c != EOF; row++, c = fgetc(file) )
        for ( ui8 col = 0; c != '\n' && c != EOF; col++, c = fgetc(file) )
            grid[ row ][ col ].value = c - 0x30;

    QueueItem queue[ 2 ][ SIZE ] = {{0}, {0}};
    ui64 queueIdx[ 2 ]           = {0, 0};

    push(queue[ Part1 ], &queueIdx[ Part1 ], (QueueItem){0});
    push(queue[ Part2 ], &queueIdx[ Part2 ], (QueueItem){0});

    while ( queueIdx[ Part1 ] > 0 || queueIdx[ Part2 ] > 0 ) {
        QueueItem current[ 2 ] = {queueIdx[ Part1 ] ? pop(queue[ Part1 ], &queueIdx[ Part1 ]) : (QueueItem){0},
                                  queueIdx[ Part2 ] ? pop(queue[ Part2 ], &queueIdx[ Part2 ]) : (QueueItem){0}};

        if ( !results[ Part1 ] && current[ Part1 ].x == cols - 1 && current[ Part1 ].y == rows - 1 )
            results[ Part1 ] = current[ Part1 ].heatLoss;

        if ( !results[ Part2 ] && current[ Part2 ].x == cols - 1 && current[ Part2 ].y == rows - 1 && current[ Part2 ].steps >= 4 )
            results[ Part2 ] = current[ Part2 ].heatLoss;

        if ( results[ Part1 ] && results[ Part2 ] )
            break;

        if ( !results[ Part1 ] && !inSeen(grid, &current[ Part1 ], Part1) ) {
            setSeen(grid, &current[ Part1 ], Part1);
            moveOnward(&current[ Part1 ], queue[ Part1 ], &queueIdx[ Part1 ], Part1);
            rotate(&current[ Part1 ], queue[ Part1 ], &queueIdx[ Part1 ], Part1);
        }

        if ( !results[ Part2 ] && !inSeen(grid, &current[ Part2 ], Part2) ) {
            setSeen(grid, &current[ Part2 ], Part2);
            moveOnward(&current[ Part2 ], queue[ Part2 ], &queueIdx[ Part2 ], Part2);
            rotate(&current[ Part2 ], queue[ Part2 ], &queueIdx[ Part2 ], Part2);
        }
    }

    for ( ui8 r = 0; r < rows; r++ )
        free(grid[ r ]);
    free(grid);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 17, &algorithm);
    return EXIT_SUCCESS;
}
