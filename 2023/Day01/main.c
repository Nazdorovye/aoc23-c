#include <stdlib.h>

#include "defines.h"

#define LIT_COUNT 10

typedef enum { one, two, both } Part;

i8* literals[ LIT_COUNT ] = {"zero\0", "one\0", "two\0", "three\0", "four\0", "five\0", "six\0", "seven\0", "eight\0", "nine\0"};

ui8 literalIndices[ LIT_COUNT ] = {};

static ui8 lineValue[ 2 ][ 2 ] = {{0, 0}, {0, 0}};
ui32 currentDigit[ 2 ]         = {0, 0};

void put(i8 digit, Part part) {
    lineValue[ part ][ currentDigit[ part ] ] = digit;
    !currentDigit[ part ] && (lineValue[ part ][ ++currentDigit[ part ] ] = lineValue[ part ][ 0 ]);
}

void setDigit(i8 digit, Part part) {
    switch ( part ) {
        case one:
        case two:  put(digit, part); break;
        case both: put(digit, one); put(digit, two);
    };
}

void resetLiteralIndices(i32 index) {
    if ( index == LIT_COUNT ) {
        for ( ui8 i = 0; i < LIT_COUNT; i++ ) {
            literalIndices[ i ] = 0;
        }
    } else {
        literalIndices[ index ] = 0;
    }
}

void algorithm(FILE* file, ui64* results) {
    resetLiteralIndices(LIT_COUNT);

    i8 c;
    while ( (c = fgetc(file)) ) {
        if ( c == '\n' || c == EOF ) {
            for ( Part part = one; part <= two; part++ ) {
                results[ part ] += 10 * lineValue[ part ][ 0 ] + lineValue[ part ][ 1 ];
                currentDigit[ part ] = lineValue[ part ][ 0 ] = lineValue[ part ][ 1 ] = 0;
            }
            resetLiteralIndices(LIT_COUNT);
            if ( c == EOF )
                break;
            continue;
        }

        if ( c >= '0' && c <= '9' ) {
            setDigit(c - 0x30, both);
            resetLiteralIndices(LIT_COUNT);
        } else {
            for ( i32 i = 0; i < LIT_COUNT; i++ ) {
                if ( c != literals[ i ][ literalIndices[ i ] ] ) {
                    literalIndices[ i ] = 0;
                    if ( c != literals[ i ][ 0 ] )
                        continue;
                }
                if ( literals[ i ][ ++literalIndices[ i ] ] == '\0' ) {
                    setDigit(i, two);
                    resetLiteralIndices(i);
                }
            }
        }
    }
}

int main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 1, &algorithm);
    return EXIT_SUCCESS;
}
