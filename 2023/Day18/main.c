#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

#include "defines.h"

typedef struct _Vertex {
    i64 x;
    i64 y;
} Vertex;

void algorithm(FILE* file, ui64* results) {
    Vertex prevVertex[ 2 ] = {{0}, {0}};

    ui64 gaussArea[ 2 ] = {0, 0};

    ui8 dir[ 2 ]     = {0, 0};
    ui32 length[ 2 ] = {0, 0};

    fscanf(file, "%c %d (#%5x%c)\n", &dir[ Part1 ], &length[ Part1 ], &length[ Part2 ], &dir[ Part2 ]);
    switch ( dir[ Part1 ] ) {
        case 'R':
            prevVertex[ Part1 ].x = length[ Part1 ];
            prevVertex[ Part1 ].y = 0;
            break;
        case 'L':
            prevVertex[ Part1 ].x = -length[ Part1 ];
            prevVertex[ Part1 ].y = 0;
            break;
        case 'D':
            prevVertex[ Part1 ].x = 0;
            prevVertex[ Part1 ].y = length[ Part1 ];
            break;
        case 'U':
            prevVertex[ Part1 ].x = 0;
            prevVertex[ Part1 ].y = -length[ Part1 ];
            break;
    }
    switch ( dir[ Part2 ] ) {
        case '0':
            prevVertex[ Part2 ].x = length[ Part2 ];
            prevVertex[ Part2 ].y = 0;
            break;
        case '1':
            prevVertex[ Part2 ].x = 0;
            prevVertex[ Part2 ].y = length[ Part2 ];
            break;
        case '2':
            prevVertex[ Part2 ].x = -length[ Part2 ];
            prevVertex[ Part2 ].y = 0;
            break;
        case '3':
            prevVertex[ Part2 ].x = 0;
            prevVertex[ Part2 ].y = -length[ Part1 ];
            break;
    }
    ui64 perimeter[ 2 ] = {length[ Part1 ], length[ Part2 ]};

    while ( fscanf(file, "%c %d (#%5x%c)\n", &dir[ Part1 ], &length[ Part1 ], &length[ Part2 ], &dir[ Part2 ]) != EOF ) {
        perimeter[ Part1 ] += length[ Part1 ];
        perimeter[ Part2 ] += length[ Part2 ];
        switch ( dir[ Part1 ] ) {
            case 'R':
                gaussArea[ Part1 ] += prevVertex[ Part1 ].x * prevVertex[ Part1 ].y;
                gaussArea[ Part1 ] -= prevVertex[ Part1 ].y * (prevVertex[ Part1 ].x + length[ Part1 ]);
                prevVertex[ Part1 ].x += length[ Part1 ];
                break;
            case 'L':
                gaussArea[ Part1 ] += prevVertex[ Part1 ].x * prevVertex[ Part1 ].y;
                gaussArea[ Part1 ] -= prevVertex[ Part1 ].y * (prevVertex[ Part1 ].x - length[ Part1 ]);
                prevVertex[ Part1 ].x -= length[ Part1 ];
                break;
            case 'D':
                gaussArea[ Part1 ] += prevVertex[ Part1 ].x * (prevVertex[ Part1 ].y + length[ Part1 ]);
                gaussArea[ Part1 ] -= prevVertex[ Part1 ].y * prevVertex[ Part1 ].x;
                prevVertex[ Part1 ].y += length[ Part1 ];
                break;
            case 'U':
                gaussArea[ Part1 ] += prevVertex[ Part1 ].x * (prevVertex[ Part1 ].y - length[ Part1 ]);
                gaussArea[ Part1 ] -= prevVertex[ Part1 ].y * prevVertex[ Part1 ].x;
                prevVertex[ Part1 ].y -= length[ Part1 ];
        }
        switch ( dir[ Part2 ] ) {
            case '0':
                gaussArea[ Part2 ] += prevVertex[ Part2 ].x * prevVertex[ Part2 ].y;
                gaussArea[ Part2 ] -= prevVertex[ Part2 ].y * (prevVertex[ Part2 ].x + length[ Part2 ]);
                prevVertex[ Part2 ].x += length[ Part2 ];
                break;
            case '1':
                gaussArea[ Part2 ] += prevVertex[ Part2 ].x * (prevVertex[ Part2 ].y + length[ Part2 ]);
                gaussArea[ Part2 ] -= prevVertex[ Part2 ].y * prevVertex[ Part2 ].x;
                prevVertex[ Part2 ].y += length[ Part2 ];
                break;
            case '2':
                gaussArea[ Part2 ] += prevVertex[ Part2 ].x * prevVertex[ Part2 ].y;
                gaussArea[ Part2 ] -= prevVertex[ Part2 ].y * (prevVertex[ Part2 ].x - length[ Part2 ]);
                prevVertex[ Part2 ].x -= length[ Part2 ];
                break;
            case '3':
                gaussArea[ Part2 ] += prevVertex[ Part2 ].x * (prevVertex[ Part2 ].y - length[ Part2 ]);
                gaussArea[ Part2 ] -= prevVertex[ Part2 ].y * prevVertex[ Part2 ].x;
                prevVertex[ Part2 ].y -= length[ Part2 ];
        }
    }
    results[ Part1 ] = gaussArea[ Part1 ] / 2 + perimeter[ Part1 ] / 2 + 1;
    results[ Part2 ] = gaussArea[ Part2 ] / 2 + perimeter[ Part2 ] / 2 + 1;
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 18, &algorithm);
    return EXIT_SUCCESS;
}
