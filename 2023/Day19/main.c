#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define inHash 28265
#define hashA  65
#define hashR  82

typedef enum __attribute__((__packed__)) _Category { o, x, m, a, s } Category;

typedef struct _Comparison {
    ui16 value : 15;
    ui8 moreThan : 1;
} Comparison;

typedef struct _Rule {
    ui32 workflowHash;
    Comparison cmp;
    Category cat;
} Rule;

typedef union _Name {
    ui8 literal[ 4 ];
    ui32 hash;
} WorkflowName;

typedef struct _Workflow {
    i32* ruleIndices;
    WorkflowName name;
} Workflow;

static ui32 pushQ(Workflow* queue, ui32* queueLength, Workflow* item) {
    if ( *queueLength && queue[ (*queueLength) - 1 ].name.hash >= item->name.hash ) {
        memmove(&queue[ (*queueLength)++ ], item, sizeof(Workflow));
        return *queueLength - 1;
    }

    if ( queue[ 0 ].name.hash <= item->name.hash ) {
        memmove(&queue[ 1 ], &queue[ 0 ], (*queueLength)++ * sizeof(Workflow));
        memmove(&queue[ 0 ], item, sizeof(Workflow));
        return 0;
    }

    i32 l = 0;
    for ( i32 r = (*queueLength) - 2, m; l <= r; ) {
        m = l + (r - l) / 2;
        if ( queue[ m ].name.hash == item->name.hash )
            break;

        if ( queue[ m ].name.hash > item->name.hash )
            l = m + 1;
        else
            r = m - 1;
    }

    memmove(&queue[ l + 1 ], &queue[ l ], ((*queueLength)++ - l) * sizeof(Workflow));
    memmove(&queue[ l ], item, sizeof(Workflow));
    return l;
}

static i32 findIndex(Workflow* queue, ui32* queueLength, ui32 hash) {
    for ( i32 l = 0, r = (*queueLength) - 1, m = l + (r - l) / 2; l <= r; m = l + (r - l) / 2 ) {
        if ( queue[ m ].name.hash == hash )
            return m;
        if ( queue[ m ].name.hash > hash )
            l = m + 1;
        else
            r = m - 1;
    }
    return -1;
}

void algorithm(FILE* file, ui64* results) {
    ui32 maxRuleCount   = 0;
    ui32 totalRuleCount = 0;

    ui32 minWorkflowHash    = UINT32_MAX;
    ui32 maxWorkflowHash    = 0;
    ui32 totalWorkflowCount = 1;

    for ( i8 c = fgetc(file), prev_c; !(c == prev_c && c == '\n'); prev_c = c, c = fgetc(file) ) {
        Workflow workflow = {0};
        for ( ui8 i = 0; c != '{'; c = fgetc(file) )
            workflow.name.literal[ i++ ] = c;
        minWorkflowHash = min(workflow.name.hash, minWorkflowHash);
        maxWorkflowHash = max(workflow.name.hash, maxWorkflowHash);
        totalWorkflowCount++;

        ui32 ruleCount = 1;
        for ( ; c != '\n'; c = fgetc(file) )
            c == ',' && ruleCount++;
        maxRuleCount = max(maxRuleCount, ruleCount);
        totalRuleCount += ruleCount;
    }
    rewind(file);

    Workflow* workflows = malloc((sizeof(Workflow) + sizeof(ui32) * maxRuleCount) * totalWorkflowCount);
    for ( ui32 i = 0; i < totalWorkflowCount; i++ )
        workflows[ i ].name.hash = 0;
    ui32 workflowIdx = 0;

    Rule* rules = malloc(sizeof(Rule) * totalRuleCount);
    memset(rules, 0, sizeof(Rule) * totalRuleCount);
    ui32 ruleIdx       = 0;
    Workflow* workflow = &workflows[ totalWorkflowCount - 1 ];

    ui8StrReader* reader = ui8StrReader_initialize(5);
    for ( i8 c = fgetc(file), prev_c; !(c == prev_c && c == '\n'); prev_c = c, c = fgetc(file) ) {
        workflow->name.hash   = 0;
        workflow->ruleIndices = malloc(sizeof(ui32) * maxRuleCount);
        for ( ui8 j = 0; j < maxRuleCount; j++ )
            workflow->ruleIndices[ j ] = -1;

        for ( ui8 i = 0; c != '{'; c = fgetc(file) )
            workflow->name.literal[ i++ ] = c;

        ui32 qIdx                 = pushQ(workflows, &workflowIdx, workflow);
        ui32 workflowRuleIdx      = 0;
        WorkflowName workflowName = {0};
        ui8 workflowNameIdx       = 0;
        ui8StrReader_clear(reader);

        for ( prev_c = c, c = fgetc(file); c != '\n'; prev_c = c, c = fgetc(file) ) {
            switch ( c ) {
                case '<': {
                    rules[ ruleIdx ].cmp.moreThan = 0;
                    switch ( reader->buffer[ 0 ] ) {
                        case 'x': rules[ ruleIdx ].cat = x; break;
                        case 'm': rules[ ruleIdx ].cat = m; break;
                        case 'a': rules[ ruleIdx ].cat = a; break;
                        case 's': rules[ ruleIdx ].cat = s;
                    }
                    ui8StrReader_clear(reader);
                    continue;
                }
                case '>': {
                    rules[ ruleIdx ].cmp.moreThan = 1;
                    switch ( reader->buffer[ 0 ] ) {
                        case 'x': rules[ ruleIdx ].cat = x; break;
                        case 'm': rules[ ruleIdx ].cat = m; break;
                        case 'a': rules[ ruleIdx ].cat = a; break;
                        case 's': rules[ ruleIdx ].cat = s;
                    }
                    ui8StrReader_clear(reader);
                    continue;
                }
                case ':': {
                    rules[ ruleIdx ].cmp.value = atoi((i8*)reader->buffer);
                    ui8StrReader_clear(reader);
                    continue;
                }
                case '}':
                case ',': {
                    workflows[ qIdx ].ruleIndices[ workflowRuleIdx++ ] = ruleIdx;
                    ui8StrReader_popString(reader, workflowName.literal);
                    rules[ ruleIdx++ ].workflowHash = workflowName.hash;
                    workflowNameIdx = workflowName.hash = 0;
                    ui8StrReader_clear(reader);
                    continue;
                }
                default: ui8StrReader_putChar(reader, c);
            }
        }
    }

    ui32 part[ 5 ] = {0};
    while ( fscanf(file, "{x=%d,m=%d,a=%d,s=%d}\n", &part[ x ], &part[ m ], &part[ a ], &part[ s ]) != EOF ) {
        ui32 workflowHash = inHash;
        for ( Rule* rule;; ) {
            i32 newIdx         = findIndex(workflows, &workflowIdx, workflowHash);
            Workflow* workflow = &workflows[ newIdx ];

            for ( ui8 i = 0, b = 0; !b && workflow->ruleIndices[ i ] != -1 && i < maxRuleCount; i++ ) {
                rule = &rules[ workflow->ruleIndices[ i ] ];
                if ( rule->cat == o )
                    break;
                for ( ui8 p = x; p <= s; p++ ) {
                    if ( rule->cat == p ) {
                        if ( rule->cmp.moreThan && part[ p ] > rule->cmp.value ) {
                            b = 1;
                            break;
                        }
                        if ( !rule->cmp.moreThan && part[ p ] < rule->cmp.value ) {
                            b = 1;
                            break;
                        }
                    }
                }
            }

            if ( rule->workflowHash == hashA ) {
                for ( ui8 p = x; p <= s; p++ )
                    results[ Part1 ] += part[ p ];
                break;
            } else if ( rule->workflowHash == hashR )
                break;

            workflowHash = rule->workflowHash;
        }
    }

    ui8StrReader_clear(reader);
    free(rules);
    for ( ui32 i = 0; i < totalWorkflowCount - 1; i++ )
        free(workflows[ i ].ruleIndices);
    free(workflows);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 19, &algorithm);
    return EXIT_SUCCESS;
}
