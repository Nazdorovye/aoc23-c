#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define MEM_GRANULE      1024
#define HAND_SIZE        5
#define DECK_UNIQUE_SIZE 13

typedef enum _ReadMode { hand, bid } ReadMode;
typedef enum _HandType { high_card, one_pair, two_pairs, three_kind, full_house, four_kind, five_kind } HandType;
typedef enum _Boolean { False, True } Boolean;

typedef struct _Hand {
    char cards[ HAND_SIZE + 1 ];
    HandType type1;
    HandType type2;
    ui32 bid;
} Hand;

ui32 cardLiteraltoIndex(i8 literal, Boolean wildcard) {
    if ( isDigit(literal) ) {
        return literal - 50 + wildcard;
    }
    switch ( literal ) {
        case 'T': return 8 + (wildcard * 1);
        case 'J': return -((wildcard * 9) - 9);
        case 'Q': return 10;
        case 'K': return 11;
        case 'A': return 12;
    }
}

int compare1(const void* a, const void* b) {
    Hand* handA = (Hand*)a;
    Hand* handB = (Hand*)b;
    if ( handA->type1 == handB->type1 ) {
        for ( ui8 i = 0; i < HAND_SIZE; i++ ) {
            if ( handA->cards[ i ] != handB->cards[ i ] )
                return cardLiteraltoIndex(handA->cards[ i ], False) - cardLiteraltoIndex(handB->cards[ i ], False);
        }
    }
    return handA->type1 - handB->type1;
}

int compare2(const void* a, const void* b) {
    Hand* handA = (Hand*)a;
    Hand* handB = (Hand*)b;
    if ( handA->type2 == handB->type2 ) {
        for ( ui8 i = 0; i < HAND_SIZE; i++ ) {
            if ( handA->cards[ i ] != handB->cards[ i ] )
                return cardLiteraltoIndex(handA->cards[ i ], True) - cardLiteraltoIndex(handB->cards[ i ], True);
        }
    }
    return handA->type2 - handB->type2;
}

static inline void setHandType(HandType* handType, ui8* uniqueCounts) {
    ui8 gotThree = 0;
    ui8 gotPair  = 0;

    for ( ui32 i = 0; i < HAND_SIZE; i++ ) {
        ui8 count = uniqueCounts[ i ];
        if ( count >= 4 ) {
            *handType = count + 1;
            break;
        }
        if ( count == 3 ) {
            if ( gotPair ) {
                *handType = full_house;
                break;
            }
            gotThree++;
            continue;
        }
        if ( count == 2 ) {
            if ( gotThree ) {
                *handType = full_house;
                break;
            }
            if ( gotPair ) {
                *handType = two_pairs;
                break;
            }
            gotPair++;
        }

        if ( *handType == high_card ) {
            if ( gotThree ) {
                *handType = three_kind;
            } else if ( gotPair ) {
                *handType = one_pair;
            }
        }
    }
}

void algorithm(FILE* file, ui64* results) {
    ui32 arrSize = MEM_GRANULE;
    Hand* hands  = malloc(sizeof(Hand) * arrSize);
    memset(hands, 0, sizeof(Hand) * arrSize);
    ui32 currentHandIdx = 0;

    ReadMode readMode = hand;
    ui32 cardRead     = 0;

    i8StrReader* numReader = i8StrReader_initialize(HAND_SIZE);

    ui32 handsCount = 0;
    for ( i8 c = getc(file), prev_c;; prev_c = c, c = getc(file) ) {
        if ( c == ' ' ) {
            readMode = !readMode;
            cardRead = 0;
            continue;
        }

        if ( c == '\n' || c == EOF ) {
            hands[ currentHandIdx ].bid = atoi(numReader->buffer);
            i8StrReader_clear(numReader);
            readMode = !readMode;
            if ( ++currentHandIdx >= arrSize ) {
                arrSize += MEM_GRANULE;
                hands = realloc(hands, arrSize);
                memset(&hands[ currentHandIdx ], 0, MEM_GRANULE);
            }
            if ( c == EOF )
                break;
            continue;
        }

        switch ( readMode ) {
            case hand:
                hands[ currentHandIdx ].cards[ cardRead++ ] = c;

                if ( cardRead == HAND_SIZE ) {
                    handsCount++;

                    ui8 uniqueCounts1[ HAND_SIZE ] = {};
                    memset(uniqueCounts1, 1, HAND_SIZE * sizeof(ui8));
                    ui32 uniqueCounts1Idx = 0;

                    ui8 uniqueCounts2[ HAND_SIZE ] = {};
                    memset(uniqueCounts2, 1, HAND_SIZE * sizeof(ui8));
                    ui32 uniqueCounts2Idx = 0;

                    ui8 jokerCount = 0;

                    ui8 visited[ HAND_SIZE ] = {};
                    memset(visited, 0, HAND_SIZE * sizeof(ui8));

                    for ( ui32 i = 0; i < HAND_SIZE; i++ ) {
                        if ( hands[ currentHandIdx ].cards[ i ] == 'J' )
                            jokerCount++;
                        if ( visited[ i ] )
                            continue;
                        for ( ui32 j = i + 1; j < HAND_SIZE; j++ ) {
                            if ( hands[ currentHandIdx ].cards[ i ] != hands[ currentHandIdx ].cards[ j ] )
                                continue;
                            if ( hands[ currentHandIdx ].cards[ i ] != 'J' )
                                uniqueCounts2[ uniqueCounts2Idx ]++;
                            uniqueCounts1[ uniqueCounts1Idx ]++;
                            visited[ j ]++;
                        }
                        uniqueCounts1Idx++;
                        uniqueCounts2Idx++;
                    }

                    setHandType(&(hands[ currentHandIdx ].type1), uniqueCounts1);
                    setHandType(&(hands[ currentHandIdx ].type2), uniqueCounts2);

                    if ( !jokerCount )
                        break;
                    if ( jokerCount == HAND_SIZE ) {
                        hands[ currentHandIdx ].type2 = five_kind;
                        break;
                    }
                    switch ( hands[ currentHandIdx ].type2 ) {
                        case high_card: {
                            hands[ currentHandIdx ].type2 += jokerCount;
                            if ( jokerCount == 2 )
                                hands[ currentHandIdx ].type2 += 1;
                            else if ( jokerCount >= 3 )
                                hands[ currentHandIdx ].type2 += 2;
                            break;
                        }
                        case one_pair: {
                            hands[ currentHandIdx ].type2 += 2;
                            if ( jokerCount >= 2 )
                                hands[ currentHandIdx ].type2 += jokerCount;
                            break;
                        }
                        case two_pairs: {
                            hands[ currentHandIdx ].type2 += 2;
                            break;
                        }
                        case three_kind: {
                            hands[ currentHandIdx ].type2 += jokerCount + 1;
                            break;
                        }
                        case four_kind: {
                            hands[ currentHandIdx ].type2++;
                        }
                    }
                }

                break;
            case bid: i8StrReader_putChar(numReader, c); break;
        }
    }

    qsort(hands, handsCount, sizeof(Hand), compare1);
    for ( ui32 i = 0; i < handsCount; i++ ) {
        results[ Part1 ] += hands[ i ].bid * (i + 1);
    }

    qsort(hands, handsCount, sizeof(Hand), compare2);
    for ( ui32 i = 0; i < handsCount; i++ ) {
        results[ Part2 ] += hands[ i ].bid * (i + 1);
    }

    free(hands);
    i8StrReader_free(numReader);
}

int main(int argc, char* argv[]) {
    openAndRun(argc, argv, 7, &algorithm);
    return EXIT_SUCCESS;
}
