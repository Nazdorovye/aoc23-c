#include <stdlib.h>

#include "defines.h"

#define RED_MAX   12
#define GREEN_MAX 13
#define BLUE_MAX  14

#define NUM_LENGTH 3

void algorithm(FILE* file, ui64* results) {
    ui32 semicolon = 0;
    ui32 gameId    = 0;
    ui8 skip       = 0;
    i8 c;

    ui32 minColors[ 3 ] = {0, 0, 0};

    i8* numReader = stackArrayInit(NUM_LENGTH, sizeof(i8), NULL);

    for ( char c = getc(file), prev_c;; prev_c = c, c = getc(file) ) {
        if ( c == '\n' || c == EOF ) {
            stackArrayClear(numReader);
            gameId++;
            !skip&& semicolon && (results[ Part1 ] += gameId);
            results[ Part2 ] += minColors[ 0 ] * minColors[ 1 ] * minColors[ 2 ];
            semicolon = skip = minColors[ 0 ] = minColors[ 1 ] = minColors[ 2 ] = 0;
            if ( c == EOF )
                break;
            continue;
        }
        if ( c == ' ' )
            continue;
        if ( c == ':' ) {
            semicolon = 1;
            continue;
        }

        if ( c >= '0' && c <= '9' && semicolon ) {
            stackArrayPushRval(numReader, c);
            continue;
        }

        switch ( c ) {
            case 'r':
                if ( prev_c != ' ' )
                    continue;
                ui32 red = atoi(numReader);
                stackArrayClear(numReader);
                (red > minColors[ 0 ]) && (minColors[ 0 ] = red);
                !skip && (red > RED_MAX) && (skip = 1);
                break;

            case 'g': {
                ui32 green = atoi(numReader);
                stackArrayClear(numReader);
                (green > minColors[ 1 ]) && (minColors[ 1 ] = green);
                !skip && (green > GREEN_MAX) && (skip = 1);
                break;
            }

            case 'b': {
                ui32 blue = atoi(numReader);
                stackArrayClear(numReader);
                (blue > minColors[ 2 ]) && (minColors[ 2 ] = blue);
                !skip && (blue > BLUE_MAX) && (skip = 1);
                break;
            }
        }
    }

    stackArrayFree(numReader);
}

int main(int argc, char* argv[]) {
    openAndRun(argc, argv, 2, &algorithm);
    return EXIT_SUCCESS;
}
