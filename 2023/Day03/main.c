#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define LINE_LENGTH 140

typedef struct _sym {
    ui32 line;
    ui32 col;
    i8 symbol;
} Symbol;

typedef struct _link {
    i32 nums[ 2 ];
    i8 symbol;
} Link;

void algorithm(FILE* file, ui64* results) {
    Link links[ LINE_LENGTH ][ LINE_LENGTH ];
    memset(&links, 0, LINE_LENGTH * LINE_LENGTH * sizeof(Link));
    Symbol mark = {};

    i8StrReader* numReader = i8StrReader_initialize(5);

    ui32 col  = 0;
    ui32 line = 0;
    fpos_t pos;
    for ( i8 c = getc(file), prev_c; c != EOF; ++col, prev_c = c, c = getc(file) ) {
        if ( prev_c == '\n' ) {
            col = 0;
            line++;
        }

        if ( c >= '0' && c <= '9' ) {
            !mark.symbol && !numReader->charIdx && col && prev_c != '.' && prev_c != '\n' &&
                ((mark.symbol = prev_c) && (mark.line = line) && (mark.col = col - 1)); // left

            !mark.symbol && !numReader->charIdx && col && line &&
                ((mark.symbol = links[ line - 1 ][ col - 1 ].symbol) && (mark.line = line - 1) &&
                 (mark.col = col - 1)); // diagonal upper left

            !mark.symbol&& line &&
                ((mark.symbol = links[ line - 1 ][ col ].symbol) && (mark.line = line - 1) && (mark.col = col)); // upper

            i8StrReader_putChar(numReader, c);
        } else if ( (prev_c >= '0' && prev_c <= '9') ) {
            !mark.symbol&& c != '.' && c != '\n' && ((mark.symbol = c) && (mark.line = line) && (mark.col = col)); // right

            !mark.symbol&& line&& c != '\n' && ((mark.symbol = links[ line - 1 ][ col ].symbol) && (mark.line = line - 1) &&
                                                (mark.col = col)); // diagonal upper right

            if ( !mark.symbol && line < LINE_LENGTH - 1 ) {
                fgetpos(file, &pos);
                fseek(file, LINE_LENGTH - numReader->charIdx - 1, SEEK_CUR);

                for ( int i = numReader->charIdx + 1; i >= 0; i-- ) {
                    c = getc(file);
                    c != '\n' && c != '.' && (c < '0' || c > '9') &&
                        ((mark.symbol = c) && (mark.line = line + 1) && (mark.col = col - i));
                    if ( mark.symbol )
                        break;
                }

                fsetpos(file, &pos);
                fseek(file, -1, SEEK_CUR);
                c = getc(file);
            }
            if ( mark.symbol ) {
                i32 num = atoi(numReader->buffer);
                i8StrReader_clear(numReader);
                results[ Part1 ] += num;

                if ( mark.symbol == '*' ) {
                    if ( links[ mark.line ][ mark.col ].nums[ 0 ] != -1 ) {
                        if ( links[ mark.line ][ mark.col ].nums[ 0 ] < 1 ) {
                            links[ mark.line ][ mark.col ].nums[ 0 ] = num;
                        } else if ( links[ mark.line ][ mark.col ].nums[ 1 ] < 1 ) {
                            links[ mark.line ][ mark.col ].nums[ 1 ] = num;
                            results[ Part2 ] += links[ mark.line ][ mark.col ].nums[ 0 ] * links[ mark.line ][ mark.col ].nums[ 1 ];
                        } else {
                            results[ Part2 ] -= links[ mark.line ][ mark.col ].nums[ 0 ] * links[ mark.line ][ mark.col ].nums[ 1 ];
                            links[ mark.line ][ mark.col ].nums[ 0 ] = -1;
                        }
                    }
                }
            } else {
                i8StrReader_clear(numReader);
            }
            mark.symbol = 0;
        }

        c != '.' && c != '\n' && (c < '0' || c > '9') && (links[ line ][ col ].symbol = c);
    }

    i8StrReader_free(numReader);
}

int main(int argc, char* argv[]) {
    openAndRun(argc, argv, 3, &algorithm);
    return EXIT_SUCCESS;
}
