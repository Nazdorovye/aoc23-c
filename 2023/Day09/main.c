#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define NUM_COUNT 21

void algorithm(FILE* file, ui64* results) {
    i64* numbers[ 2 ];
    for ( ui8 part = Part1; part <= Part2; part++ ) {
        numbers[ part ] = malloc(NUM_COUNT * sizeof(i64));
        memset(numbers[ part ], 0, NUM_COUNT * sizeof(i64));
    }

    ui32 currentNumberIdx = 0;
    ui32 numbersLength    = NUM_COUNT;

    i8StrReader* numReader = i8StrReader_initialize(10);
    char polarity          = 1;

    for ( char c = fgetc(file);; c = fgetc(file) ) {
        if ( c == '-' ) {
            polarity = -1;
            continue;
        }
        if ( isDigit(c) ) {
            i8StrReader_putChar(numReader, c);
            continue;
        }

        i64 currentNumber = atol(numReader->buffer);
        i8StrReader_clear(numReader);
        numbers[ Part1 ][ currentNumberIdx ] = currentNumber * polarity;
        currentNumberIdx++;
        polarity = 1;

        if ( c == '\n' || c == EOF ) {
            for ( ui32 i1 = currentNumberIdx - 1, i2 = 0; i2 < currentNumberIdx; i1--, i2++ )
                numbers[ Part2 ][ i2 ] = numbers[ Part1 ][ i1 ];

            for ( ui8 part = Part1; part <= Part2; part++ ) {
                for ( ui32 count0 = 0, dl = currentNumberIdx - 1;; dl--, count0 = 0 ) {
                    for ( ui32 i = 0; i < dl; i++ ) {
                        numbers[ part ][ i ] = numbers[ part ][ i + 1 ] - numbers[ part ][ i ];
                        if ( !numbers[ part ][ i ] )
                            count0++;
                    }
                    results[ part ] += numbers[ part ][ dl ];
                    if ( count0 == dl )
                        break;
                }
            }

            currentNumberIdx = 0;
            memset(numbers[ Part1 ], 0, NUM_COUNT * sizeof(i64));
            if ( c == EOF )
                break;
            continue;
        }

        if ( currentNumberIdx >= numbersLength ) {
            numbersLength += NUM_COUNT;
            for ( ui8 part = Part1; part <= Part2; part++ )
                numbers[ part ] = realloc(numbers[ part ], numbersLength * sizeof(i64));
        }
    }

    i8StrReader_clear(numReader);
}

i32 main(i32 argc, char* argv[]) {
    openAndRun(argc, argv, 9, &algorithm);
    return EXIT_SUCCESS;
}
