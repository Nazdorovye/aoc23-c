#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define BOX_COUNT    256
#define LABEL_LEN    16
#define LENS_PER_BOX 16

typedef struct _Lens {
    ui8 label[ LABEL_LEN ];
    ui8 focalLength;
} Lens;

ui8 hash(i8* str, ui64 len) {
    ui64 hash = 0;
    for ( ui64 i = 0; i < len; i++ ) {
        hash += str[ i ];
        hash *= 17;
        hash %= 256;
    }
    return (ui8)hash;
}

void algorithm(FILE* file, ui64* results) {
    Lens boxes[ BOX_COUNT ][ LENS_PER_BOX ];
    memset(boxes, 0, sizeof(Lens) * BOX_COUNT * LENS_PER_BOX);

    ui8StrReader* str[ 2 ] = {ui8StrReader_initialize(16), ui8StrReader_initialize(16)};

    for ( i8 c = fgetc(file);; c = fgetc(file) ) {
        if ( c == ',' || c == EOF ) {
            results[ Part1 ] += hash(str[ Part1 ]->buffer, str[ Part1 ]->charIdx);
            ui8StrReader_clear(str[ Part1 ]);
            ui8StrReader_clear(str[ Part2 ]);
            if ( c == EOF )
                break;
            continue;
        }
        ui8StrReader_putChar(str[ Part1 ], c);

        if ( c == '-' || c == '=' ) {
            ui8 h = hash(str[ Part2 ]->buffer, str[ Part2 ]->charIdx);

            if ( c == '=' ) {
                ui8 focalLength = fgetc(file);
                ui8StrReader_putChar(str[ Part1 ], focalLength);

                ui8 good = 0;
                for ( ui16 l = 0; l < LENS_PER_BOX; l++ ) {
                    if ( boxes[ h ][ l ].label[ 0 ] == 0 ) {
                        memcpy(boxes[ h ][ l ].label, str[ Part2 ]->buffer, str[ Part2 ]->charIdx);
                        boxes[ h ][ l ].focalLength = focalLength;
                        good++;
                        break;
                    }

                    if ( strcmp(boxes[ h ][ l ].label, str[ Part2 ]->buffer) )
                        continue;
                    boxes[ h ][ l ].focalLength = focalLength;
                    good++;
                    break;
                }
                if ( !good ) {
                    printf("Increase LENS_PER_BOX! Aborting.\n");
                    exit(EXIT_FAILURE);
                }
            } else {
                i8 lensIdx = -1;
                for ( ui16 l = 0; l < LENS_PER_BOX; l++ ) {
                    if ( boxes[ h ][ l ].label[ 0 ] == 0 )
                        break;
                    if ( strcmp(boxes[ h ][ l ].label, str[ Part2 ]->buffer) )
                        continue;
                    lensIdx = l;
                    break;
                }
                if ( lensIdx >= 0 ) {
                    memmove(&boxes[ h ][ lensIdx ], &boxes[ h ][ lensIdx + 1 ], (LENS_PER_BOX - lensIdx - 1) * sizeof(Lens));
                }
            }
            continue;
        }
        ui8StrReader_putChar(str[ Part2 ], c);
    }

    for ( ui16 b = 0; b < BOX_COUNT; b++ ) {
        for ( ui16 l = 0; l < LENS_PER_BOX; l++ ) {
            if ( !boxes[ b ][ l ].label[ 0 ] )
                break;
            results[ Part2 ] += (b + 1) * (l + 1) * (boxes[ b ][ l ].focalLength - 0x30);
        }
    }

    for ( ui8 part = Part1; part <= Part2; part++ )
        ui8StrReader_free(str[ part ]);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 15, &algorithm);
    return EXIT_SUCCESS;
}
