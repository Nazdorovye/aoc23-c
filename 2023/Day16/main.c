#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

typedef enum _TileMod { tmNone = '.', tmBackslash = '\\', tmForwslash = '/', tmVbar = '|', tmHbar = '-' } TileMod;
typedef enum _Mod { mNone, mBackslash, mForwslash, mVbar, mHbar } Mod;
typedef enum _DirIdx { U, D, L, R } DirIdx;
typedef enum _State { end, off, on } State;

typedef struct _Tile {
    ui8 u : 1;
    ui8 d : 1;
    ui8 l : 1;
    ui8 r : 1;
    ui8 counted : 1;
    ui8 mod : 3;
} Tile;

typedef struct _Dir {
    ui8 state : 2;
    ui8 lit : 2;
    i8 sign : 2;
    ui8 x : 1;
    ui8 y : 1;
} Dir;

static Dir dirs[ 4 ] = {[U] = {.lit = (ui8)U, .x = 0, .y = 1, .sign = -1, .state = on},
                        [D] = {.lit = (ui8)D, .x = 0, .y = 1, .sign = 1, .state = on},
                        [L] = {.lit = (ui8)L, .x = 1, .y = 0, .sign = -1, .state = on},
                        [R] = {.lit = (ui8)R, .x = 1, .y = 0, .sign = 1, .state = on}};

typedef struct _Beam {
    ui8 x;
    ui8 y;
    Dir dir;
} Beam;

static inline ui8 isNthBitSet(ui8* c, ui8 n) {
    return *c & ((ui8)1 << n);
}

static inline void setNthBit(ui8* c, ui8 n, ui8 set) {
    *c = (*c & ~((ui8)1 << n)) | ((ui8)set << n);
}

ui64 countEnergy(Beam* beams, Tile** contraption, ui8 cols, ui8 rows, Beam start) {
    memset(beams, 0, sizeof(Beam) * cols * rows);
    beams[ 0 ]    = start;
    ui16 nextBeam = 1;

    for ( ui8 r = 0; r < rows; r++ ) {
        for ( ui8 c = 0; c < cols; c++ ) {
            Mod temp = contraption[ r ][ c ].mod;
            memset(&contraption[ r ][ c ], 0, sizeof(Tile));
            contraption[ r ][ c ].mod = temp;

            if ( r != beams[ 0 ].y || c != beams[ 0 ].x )
                continue;
            contraption[ beams[ 0 ].y ][ beams[ 0 ].x ].counted++;
        }
    }

    ui64 result = 1;
    for ( ui16 b = 0; b < nextBeam; b++ ) {
        while ( beams[ b ].dir.state == on ) {
            // set new beam direction
            switch ( contraption[ beams[ b ].y ][ beams[ b ].x ].mod ) {
                case mBackslash: {
                    beams[ b ].dir.x   = !beams[ b ].dir.x;
                    beams[ b ].dir.y   = !beams[ b ].dir.y;
                    beams[ b ].dir.lit = beams[ b ].dir.y * (D * (beams[ b ].dir.sign > 0) + U * (beams[ b ].dir.sign < 0)) +
                                         beams[ b ].dir.x * (R * (beams[ b ].dir.sign > 0) + L * (beams[ b ].dir.sign < 0));
                    break;
                }
                case mForwslash: {
                    ui8 tempX = beams[ b ].dir.x;
                    beams[ b ].dir.sign *= -1;
                    beams[ b ].dir.x   = beams[ b ].dir.y;
                    beams[ b ].dir.y   = tempX;
                    beams[ b ].dir.lit = beams[ b ].dir.y * (D * (beams[ b ].dir.sign > 0) + U * (beams[ b ].dir.sign < 0)) +
                                         beams[ b ].dir.x * (R * (beams[ b ].dir.sign > 0) + L * (beams[ b ].dir.sign < 0));
                    break;
                }
                case mVbar: {
                    if ( beams[ b ].dir.x == 0 )
                        break; // moving vertically - nothing to change
                    beams[ nextBeam ]       = beams[ b ];
                    beams[ nextBeam++ ].dir = dirs[ D ];
                    beams[ b ].dir          = dirs[ U ];
                    break;
                }
                case mHbar: {
                    if ( beams[ b ].dir.y == 0 )
                        break; // moving horizontally - nothing to change
                    beams[ nextBeam ]       = beams[ b ];
                    beams[ nextBeam++ ].dir = dirs[ L ];
                    beams[ b ].dir          = dirs[ R ];
                }
            }

            // check if tile already passed with a beam of same direction
            if ( isNthBitSet((ui8*)&contraption[ beams[ b ].y ][ beams[ b ].x ], beams[ b ].dir.lit) ) {
                beams[ b ].dir.state = off;
                break;
            }

            // set marker that tile is being passed with a beam of same direction
            setNthBit((ui8*)&contraption[ beams[ b ].y ][ beams[ b ].x ], beams[ b ].dir.lit, 1);

            i16 nx = beams[ b ].x + (beams[ b ].dir.x * beams[ b ].dir.sign);
            i16 ny = beams[ b ].y + (beams[ b ].dir.y * beams[ b ].dir.sign);

            // check if in contraption bounds
            if ( nx < 0 || nx >= cols || ny < 0 || ny >= rows ) {
                beams[ b ].dir.state = off;
                break;
            }

            // set new beam position
            beams[ b ].x = nx;
            beams[ b ].y = ny;

            if ( !contraption[ ny ][ nx ].counted ) {
                contraption[ ny ][ nx ].counted = 1;
                result++;
            }
        }
    }
    return result;
}

void algorithm(FILE* file, ui64* results) {
    ui8 cols = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file) )
        cols++;
    fseek(file, 0, SEEK_END);
    ui8 rows = (ftell(file) + 1) / (cols + 1);
    rewind(file);

    Tile** contraption = malloc(sizeof(Tile*) * rows);
    for ( ui8 r = 0; r < rows; r++ ) {
        contraption[ r ] = malloc(sizeof(Tile) * cols);
        memset(contraption[ r ], 0, sizeof(Tile) * cols);
    }

    for ( i8 c = fgetc(file), row = 0; c != EOF; row++, c = fgetc(file) )
        for ( ui8 col = 0; c != '\n' && c != EOF; col++, c = fgetc(file) )
            switch ( c ) {
                case tmNone:      contraption[ row ][ col ].mod = mNone; break;
                case tmBackslash: contraption[ row ][ col ].mod = mBackslash; break;
                case tmForwslash: contraption[ row ][ col ].mod = mForwslash; break;
                case tmVbar:      contraption[ row ][ col ].mod = mVbar; break;
                case tmHbar:      contraption[ row ][ col ].mod = mHbar;
            }

    Beam* beams = malloc(sizeof(Beam) * cols * rows);

    for ( i16 r = 0; r < rows; r++ )
        for ( i16 c = 0; c < cols; c += cols - 1 ) {
            ui64 result = countEnergy(beams, contraption, cols, rows, (Beam){.x = c, .y = r, .dir = c ? dirs[ L ] : dirs[ R ]});
            if ( result > results[ Part2 ] )
                results[ Part2 ] = result;
            if ( r == 0 && c == 0 )
                results[ Part1 ] = results[ Part2 ];
        }

    for ( i16 c = 1; c < cols - 1; c++ )
        for ( i16 r = 0; r < rows; r += rows - 1 ) {
            ui64 result = countEnergy(beams, contraption, cols, rows, (Beam){.x = c, .y = r, .dir = r ? dirs[ U ] : dirs[ D ]});
            if ( result > results[ Part2 ] )
                results[ Part2 ] = result;
        }

    free(beams);
    for ( ui8 r = 0; r < rows; r++ )
        free(contraption[ r ]);
    free(contraption);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 16, &algorithm);
    return EXIT_SUCCESS;
}
