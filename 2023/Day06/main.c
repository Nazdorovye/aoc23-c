#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define NUM_LENGTH      10
#define LONG_NUM_LENGTH 19

void algorithm(FILE* file, ui64* results) {
    ui32 gameCount = 0;
    for ( i8 c = getc(file), prev_c;; prev_c = c, c = getc(file) ) {
        if ( (c == ' ' || c == '\n') && isDigit(prev_c) )
            gameCount++;
        if ( c == '\n' )
            break;
    }
    rewind(file);

    ui32* games = malloc(sizeof(ui32) * gameCount);
    memset(games, 0, sizeof(ui32) * gameCount);
    ui8 gameIdx = 0;

    i8StrReader* numReader = i8StrReader_initialize(NUM_LENGTH);
    i8StrReader* longT     = i8StrReader_initialize(LONG_NUM_LENGTH);
    i8StrReader* longD     = i8StrReader_initialize(LONG_NUM_LENGTH);

    i32 line         = 0;
    results[ Part1 ] = 1;
    for ( i8 c = getc(file), prev_c;; prev_c = c, c = getc(file) ) {
        if ( isDigit(c) ) {
            i8StrReader_putChar(numReader, c);
            if ( !line )
                i8StrReader_putChar(longT, c);
            else
                i8StrReader_putChar(longD, c);
            continue;
        }

        if ( numReader->charIdx ) {
            if ( !line ) {
                games[ gameIdx++ ] = atoi(numReader->buffer);
            } else {
                ui32 c                  = atoi(numReader->buffer);
                QuadEqResult_f32 result = calcQuadEq_i32(1, -games[ gameIdx++ ], c);
                if ( result.resultCount == 2 ) {
                    results[ Part1 ] *= (i32)ceil(result.x1) - (i32)floor(result.x2) - 1;
                } else if ( result.resultCount == 1 ) {
                    results[ Part1 ] *= (i32)result.x1;
                } else {
                    printf("Game #%d :: FATAL :: No results! Breaking\n", gameIdx);
                    line = -1;
                    break;
                }
            }
            i8StrReader_clear(numReader);
        }

        if ( c == '\n' ) {
            line++;
            gameIdx = 0;
        }
        if ( c == EOF )
            break;
    }

    if ( line < 0 ) {
        free(games);
        return;
    }

    ui64 longGame[ 2 ]          = {0, 0};
    longGame[ 0 ]               = atol(longT->buffer);
    longGame[ 1 ]               = atol(longD->buffer);
    results[ Part2 ]            = 1;
    QuadEqResult_f64 longResult = calcQuadEq_i64(1, -longGame[ 0 ], longGame[ 1 ]);
    if ( longResult.resultCount == 2 ) {
        results[ Part2 ] *= (i64)ceil(longResult.x1) - (i64)floor(longResult.x2) - 1;
    } else if ( longResult.resultCount == 1 ) {
        results[ Part2 ] *= (i64)longResult.x1;
    } else {
        printf("Long game :: FATAL :: No results!\n");
    }

    free(games);
    i8StrReader_clear(numReader);
    i8StrReader_clear(longT);
    i8StrReader_clear(longD);
}

int main(int argc, i8* argv[]) {
    openAndRun(argc, argv, 6, &algorithm);
    return EXIT_SUCCESS;
}
