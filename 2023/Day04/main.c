#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define NUM_LENGTH 2

typedef enum _mode { cardNum = '\n', winNum = ':', haveNum = '|' } Mode;

void algorithm(FILE* file, ui64* results) {
    Mode mode        = cardNum;
    ui32 winNumCount = 0;
    ui32 lineLength  = 1;
    for ( i8 c = getc(file), prev_c; c != '\n'; lineLength++, prev_c = c, c = getc(file) ) {
        c == winNum && (mode = winNum);
        c == haveNum && (mode = haveNum);
        isDigit(prev_c) && c == ' ' && mode == winNum&& winNumCount++;
    }
    rewind(file);
    mode = cardNum;

    fseek(file, 0, SEEK_END);
    ui32 fsize = ftell(file) + 1;
    rewind(file);
    ui32 cardCount = fsize / lineLength;

    i8StrReader* numReader = i8StrReader_initialize(NUM_LENGTH);

    ui32* winNumbers = malloc(sizeof(ui32) * winNumCount);
    memset(winNumbers, 0, sizeof(*winNumbers) * winNumCount);
    ui32 winNumIdx = 0;

    ui32* cardCopies = malloc(sizeof(ui32) * cardCount);
    for ( ui32 i = 0; i < cardCount; i++ )
        cardCopies[ i ] = 1;

    ui32 cardNumber = 0;
    ui32 matchCount = 0;
    ui32 mod        = 0;

    for ( i8 c = getc(file), prev_c;; prev_c = c, c = getc(file) ) {
        if ( isDigit(c) && mode != cardNum ) {
            i8StrReader_putChar(numReader, c);
            continue;
        }

        if ( numReader->charIdx ) {
            i8 number = atoi(numReader->buffer);
            i8StrReader_clear(numReader);

            switch ( mode ) {
                case winNum: winNumbers[ winNumIdx++ ] = number; break;
                case haveNum:
                    for ( int n = 0; n < winNumCount; n++ ) {
                        if ( winNumbers[ n ] == 0 )
                            break;
                        if ( winNumbers[ n ] != number )
                            continue;
                        if ( cardNumber + ++matchCount < cardCount ) {
                            cardCopies[ cardNumber + matchCount ] += 1 * cardCopies[ cardNumber ];
                        }
                        mod = (mod == 0) ? 1 : mod * 2;
                        break;
                    }
            }
        }

        switch ( c ) {
            case cardNum: cardNumber++;
            case winNum:
            case haveNum:
                mode = c;
                results[ Part1 ] += mod;
                matchCount = winNumIdx = mod = 0;
                break;
        }
        if ( c == EOF )
            break;
    }

    for ( ui32 i = 0; i < cardCount; i++ )
        results[ Part2 ] += cardCopies[ i ];

    i8StrReader_free(numReader);
    free(cardCopies);
    free(winNumbers);
}

int main(int argc, char* argv[]) {
    openAndRun(argc, argv, 4, &algorithm);
    return EXIT_SUCCESS;
}
