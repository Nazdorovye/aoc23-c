#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define DST 0
#define SRC 1
#define RNG 2

#define MAP_COUNT 7

#define NUM_LENGTH 10

typedef struct _Mutator {
    ui64 dst;
    ui64 src;
    ui64 rng;
} Mutator;

typedef struct _Range {
    i64 targetMapIdx;
    i64 targetMutatorIdx;
    ui64 min;
    ui64 max;
} Range;

#define HEAP_SIZE 64
static Range rangeHeap[ HEAP_SIZE ] = {};

static inline void initRangeHeap() {
    for ( ui32 i = 0; i < HEAP_SIZE; i++ )
        rangeHeap[ i ].targetMapIdx = -1;
}

static inline Range* getEmptyRange(ui32 targetMapIdx, ui32 targetMutatorIdx) {
    for ( ui32 i = 0; i < HEAP_SIZE; i++ ) {
        if ( rangeHeap[ i ].targetMapIdx == -1 ) {
            rangeHeap[ i ].targetMapIdx     = targetMapIdx;
            rangeHeap[ i ].targetMutatorIdx = targetMutatorIdx;
            return &rangeHeap[ i ];
        }
    }
    return NULL;
}

void algorithm(FILE* file, ui64* results) {
    ui8 seedCount                      = 0;
    ui32 mapsMutatorCount[ MAP_COUNT ] = {};
    memset(mapsMutatorCount, 0, MAP_COUNT * sizeof(ui32));
    i32 currentMapIdx = -1;

    for ( i8 c = getc(file); c != '\n'; c = getc(file) )
        if ( c == ' ' )
            seedCount++;

    for ( i8 c = getc(file), prev_c;; prev_c = c, c = getc(file) ) {
        if ( prev_c == ':' )
            currentMapIdx++;
        if ( c == '\n' && isDigit(prev_c) )
            mapsMutatorCount[ currentMapIdx ]++;
        else if ( c == EOF ) {
            if ( isDigit(prev_c) )
                mapsMutatorCount[ currentMapIdx ]++;
            break;
        }
    }
    rewind(file);

    i8StrReader* numReader = i8StrReader_initialize(NUM_LENGTH);

    ui32* seeds = malloc(seedCount * sizeof(ui32));
    memset(seeds, 0, seedCount * sizeof(ui32));
    ui32 currentSeedIdx = 0;

    Mutator** mutators = malloc(MAP_COUNT * sizeof(Mutator*));
    for ( ui32 i = 0; i < MAP_COUNT; i++ ) {
        mutators[ i ] = malloc(mapsMutatorCount[ i ] * sizeof(Mutator));
        memset(mutators[ i ], 0, mapsMutatorCount[ i ] * sizeof(Mutator));
    }
    currentMapIdx          = -1;
    ui32 currentMutatorIdx = 0;
    ui32 readMutator       = 0;

    ui64 line           = 0;
    ui64 m[ MAP_COUNT ] = {};
    memset(m, 0, MAP_COUNT * sizeof(ui64));
    for ( i8 c = getc(file), prev_c;; prev_c = c, c = getc(file) ) {
        if ( isDigit(c) ) {
            i8StrReader_putChar(numReader, c);
            continue;
        }

        if ( numReader->charIdx ) {
            if ( !line ) {
                seeds[ currentSeedIdx++ ] = atol(numReader->buffer);
            } else {
                switch ( readMutator++ ) {
                    case DST: mutators[ currentMapIdx ][ currentMutatorIdx ].dst = atol(numReader->buffer); break;
                    case SRC: mutators[ currentMapIdx ][ currentMutatorIdx ].src = atol(numReader->buffer); break;
                    case RNG: mutators[ currentMapIdx ][ currentMutatorIdx++ ].rng = atol(numReader->buffer); break;
                }
            }
            i8StrReader_clear(numReader);
        }

        if ( line && prev_c == ':' ) {
            currentMapIdx++;
            currentMutatorIdx = 0;
        }

        if ( c == '\n' ) {
            readMutator = 0;
            line++;
        } else if ( c == EOF )
            break;
    }

    currentMutatorIdx = 0;
    results[ Part1 ]  = -1;

    for ( currentSeedIdx = 0; currentSeedIdx < seedCount; currentSeedIdx++ ) {
        ui32 low = seeds[ currentSeedIdx ];

        for ( currentMapIdx = 0; currentMapIdx < MAP_COUNT; currentMapIdx++ ) {
            for ( currentMutatorIdx = 0; currentMutatorIdx < mapsMutatorCount[ currentMapIdx ]; currentMutatorIdx++ ) {
                Mutator currentMutator = mutators[ currentMapIdx ][ currentMutatorIdx ];

                if ( low < currentMutator.src || low > currentMutator.src + currentMutator.rng )
                    continue;
                low = low - currentMutator.src + currentMutator.dst;
                break;
            }
        }
        if ( low < results[ Part1 ] )
            results[ Part1 ] = low;
    }

    results[ Part2 ] = -1;
    for ( currentSeedIdx = 0; currentSeedIdx < seedCount; currentSeedIdx++ ) {
        initRangeHeap();
        rangeHeap[ 0 ].min              = seeds[ currentSeedIdx ];
        rangeHeap[ 0 ].max              = seeds[ currentSeedIdx++ ] + seeds[ currentSeedIdx ] - 1;
        rangeHeap[ 0 ].targetMapIdx     = 0;
        rangeHeap[ 0 ].targetMutatorIdx = 0;

        for ( currentMapIdx = 0; currentMapIdx < MAP_COUNT; currentMapIdx++ ) {
            for ( ui32 rangeIdx = 0;; rangeIdx++ ) {
                Range* currentRange = &rangeHeap[ rangeIdx ];
                if ( currentRange->targetMapIdx == -1 )
                    break;

                for ( currentMutatorIdx = 0; currentMutatorIdx < mapsMutatorCount[ currentMapIdx ]; currentMutatorIdx++ ) {
                    Mutator currentMutator = mutators[ currentMapIdx ][ currentMutatorIdx ];
                    if ( currentRange->targetMutatorIdx > currentMutatorIdx ) {
                        currentRange->targetMutatorIdx = 0;
                        continue;
                    }

                    ui64 max = currentMutator.src + currentMutator.rng;

                    if ( currentRange->min < currentMutator.src && currentRange->max < currentMutator.src ) {
                        continue;
                    }
                    if ( currentRange->min > max && currentRange->max > max ) {
                        continue;
                    }

                    // fully overlaps
                    if ( currentRange->min >= currentMutator.src && currentRange->max <= max ) {
                        currentRange->min = currentRange->min - currentMutator.src + currentMutator.dst;
                        currentRange->max = currentRange->max - currentMutator.src + currentMutator.dst;
                        break;
                    }

                    Range* newRng = getEmptyRange(currentMapIdx + 1, currentMutatorIdx + 1);

                    // only seed ending overlaps
                    if ( currentRange->min < currentMutator.src && currentRange->max >= currentMutator.src ) {
                        newRng->min       = currentRange->min;
                        newRng->max       = currentMutator.src - 1;
                        currentRange->min = currentMutator.dst;
                        currentRange->max = currentRange->max - currentMutator.src + currentMutator.dst;
                        break;
                    }

                    // only seed start overlaps
                    if ( currentRange->min <= max && currentRange->max > max ) {
                        newRng->min       = max + 1;
                        newRng->max       = currentRange->max;
                        currentRange->min = currentRange->min - currentMutator.src + currentMutator.dst;
                        currentRange->max = max - currentMutator.src + currentMutator.dst;
                        break;
                    }

                    // seed overlap in middle
                    Range* newRng2    = getEmptyRange(currentMapIdx + 1, currentMutatorIdx + 1);
                    newRng->min       = currentRange->min;
                    newRng->max       = currentMutator.src - 1;
                    newRng2->min      = max + 1;
                    newRng2->max      = currentRange->max;
                    currentRange->min = currentMutator.dst;
                    currentRange->max = max - currentMutator.src + currentMutator.dst;
                }
                currentRange->targetMapIdx = currentMapIdx + 1;
            }
        }

        for ( ui32 rangeIdx = 0; rangeIdx < HEAP_SIZE; rangeIdx++ ) {
            if ( rangeHeap[ rangeIdx ].targetMapIdx < 0 )
                break;
            if ( rangeHeap[ rangeIdx ].min < results[ Part2 ] )
                results[ Part2 ] = rangeHeap[ rangeIdx ].min;
        }
    }

    i8StrReader_free(numReader);
    for ( currentMapIdx = 0; currentMapIdx < MAP_COUNT; currentMapIdx++ )
        free(mutators[ currentMapIdx ]);
    free(mutators);
    free(seeds);
}

int main(int argc, i8* argv[]) {
    openAndRun(argc, argv, 5, &algorithm);
    return EXIT_SUCCESS;
}
