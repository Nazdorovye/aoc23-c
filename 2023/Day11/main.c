#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define HEAP_SIZE 512
#define GLX       '#'

#define EXP_SMALL 1
#define EXP_LARGE 1000000

typedef union _Coord {
    struct _Verb {
        ui16 l;
        ui16 c;
    } _v;
    ui16 _[ 2 ];
} Coord;

void algorithm(FILE* file, ui64* results) {
    ui16 cols = 0;
    for ( i8 c = fgetc(file); c != '\n'; c = fgetc(file) )
        cols++;
    fseek(file, 0, SEEK_END);
    ui16 lines = (ftell(file) + 1) / (cols + 1);
    rewind(file);

    ui16* glxCountPerCol = malloc(sizeof(ui16) * cols);
    memset(glxCountPerCol, 0, sizeof(ui16) * cols);
    ui16* glxCountPerLine = malloc(sizeof(ui16) * lines);
    memset(glxCountPerLine, 0, sizeof(ui16) * lines);

    ui16 arrSize          = min(cols * lines, HEAP_SIZE);
    Coord* galaxies       = malloc(sizeof(Coord) * arrSize);
    ui16 currentGalaxyIdx = 0;

    ui16 line = 0;
    ui16 col  = 0;
    for ( i8 c = fgetc(file); c != EOF; c = fgetc(file) ) {
        if ( c == '\n' ) {
            col = 0;
            line++;
            continue;
        }
        if ( c == GLX ) {
            glxCountPerCol[ col ]++;
            glxCountPerLine[ line ]++;
            galaxies[ currentGalaxyIdx ]._v.c   = col;
            galaxies[ currentGalaxyIdx++ ]._v.l = line;
            if ( currentGalaxyIdx >= arrSize ) {
                arrSize += min(cols * lines, HEAP_SIZE);
                galaxies = realloc(galaxies, sizeof(Coord) * arrSize);
            }
        }
        col++;
    }

    for ( ui16 i = 0; i < currentGalaxyIdx - 1; i++ ) {
        Coord glx1 = galaxies[ i ];
        for ( ui16 j = i + 1; j < currentGalaxyIdx; j++ ) {
            Coord glx2 = galaxies[ j ];
            ui64 eMod  = 0; // expansion modifier

            ui16 _max, _min;
            if ( glx1._v.c > glx2._v.c ) {
                _max = glx1._v.c;
                _min = glx2._v.c;
            } else {
                _max = glx2._v.c;
                _min = glx1._v.c;
            }

            for ( ui16 k = _min + 1; k < _max; k++ )
                if ( !glxCountPerCol[ k ] )
                    eMod++;

            if ( glx1._v.l > glx2._v.l ) {
                _max = glx1._v.l;
                _min = glx2._v.l;
            } else {
                _max = glx2._v.l;
                _min = glx1._v.l;
            }

            for ( ui16 k = _min + 1; k < _max; k++ )
                if ( !glxCountPerLine[ k ] )
                    eMod++;

            ui64 r = abs(galaxies[ j ]._v.c - galaxies[ i ]._v.c) + abs(galaxies[ j ]._v.l - galaxies[ i ]._v.l);
            results[ Part1 ] += r + eMod * EXP_SMALL;
            results[ Part2 ] += r + eMod * (EXP_LARGE - 1);
        }
    }

    free(galaxies);
    free(glxCountPerLine);
    free(glxCountPerCol);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 11, &algorithm);
    return EXIT_SUCCESS;
}
