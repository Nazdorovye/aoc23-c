#include <stdlib.h>

#include "defines.h"

#define MAX_STR_LENGTH   256
#define HELPER_COUNT     128
#define PART2_COPY_TIMES 4

i64 cache[ MAX_STR_LENGTH ][ HELPER_COUNT ];

typedef enum _ReadMode { springs, helpers } ReadMode;

void clearCache() {
    for ( ui16 i = 0; i < MAX_STR_LENGTH; i++ )
        for ( ui16 j = 0; j < HELPER_COUNT; j++ )
            cache[ i ][ j ] = -1;
}

ui64 parser(ui8* helpers, i8 helperCount, ui8* springs, i8 springCount) {
    if ( springCount <= 0 ) {
        if ( helperCount <= 0 )
            return 1;
        return 0;
    }

    if ( helperCount <= 0 ) {
        for ( ui8 i = 0; i < springCount; i++ )
            if ( springs[ i ] == '#' )
                return 0;
        return 1;
    }

    if ( cache[ helperCount ][ springCount ] != -1 ) {
        return cache[ helperCount ][ springCount ];
    }

    ui64 result = 0;
    if ( springs[ 0 ] == '.' || springs[ 0 ] == '?' )
        result += parser(helpers, helperCount, &springs[ 1 ], springCount - 1);

    if ( springs[ 0 ] == '#' || springs[ 0 ] == '?' ) {
        if ( helpers[ 0 ] <= springCount ) {
            for ( ui8 i = 0; i < helpers[ 0 ]; i++ )
                if ( springs[ i ] == '.' ) {
                    cache[ helperCount ][ springCount ] = result;
                    return result;
                }

            if ( helpers[ 0 ] == springCount || springs[ helpers[ 0 ] ] != '#' )
                result += parser(&helpers[ 1 ], helperCount - 1, &springs[ helpers[ 0 ] + 1 ], springCount - helpers[ 0 ] - 1);
        }
    }

    cache[ helperCount ][ springCount ] = result;
    return result;
}

void algorithm(FILE* file, ui64* results) {
    ui8StrReader* springsReader = ui8StrReader_initialize(MAX_STR_LENGTH);
    ui8StrReader* numReader     = ui8StrReader_initialize(10);

    ui8 helpers[ HELPER_COUNT ];
    ui8 helperIdx = 0;

    ReadMode mode = springs;
    for ( i8 c = getc(file);; c = getc(file) ) {
        if ( c == ' ' ) {
            mode = !mode;
            continue;
        }
        if ( c == ',' || c == '\n' || c == EOF ) {
            helpers[ helperIdx++ ] = atoi(numReader->buffer);
            ui8StrReader_clear(numReader);
            if ( c == ',' )
                continue;
        }
        if ( c == '\n' || c == EOF ) {
            clearCache();
            results[ Part1 ] += parser(helpers, helperIdx, springsReader->buffer, springsReader->charIdx);

            for ( ui64 i = 0, sz = springsReader->charIdx, hz = helperIdx; i < PART2_COPY_TIMES;
                  springsReader->charIdx += sz, helperIdx += hz, i++ ) {
                ui8StrReader_putChar(springsReader, '?');
                memcpy(&springsReader->buffer[ springsReader->charIdx ], &springsReader->buffer[ 0 ], sz);
                memcpy(&helpers[ helperIdx ], &helpers[ 0 ], hz);
            }

            clearCache();
            results[ Part2 ] += parser(helpers, helperIdx, springsReader->buffer, springsReader->charIdx);

            ui8StrReader_clear(springsReader);
            ui8StrReader_clear(numReader);
            helperIdx = 0;
            mode      = !mode;
            if ( c == '\n' )
                continue;
            break;
        }
        if ( mode == springs )
            ui8StrReader_putChar(springsReader, c);
        else
            ui8StrReader_putChar(numReader, c);
    }

    ui8StrReader_free(numReader);
    ui8StrReader_free(springsReader);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 12, &algorithm);
    return EXIT_SUCCESS;
}
