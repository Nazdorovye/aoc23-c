#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define W_MAX    64
#define ROW_MULT 100

void modBit(ui32* n, ui8 pos, ui8 bit) {
    bit       = (bit > 0);
    ui32 mask = 1 << pos;
    *n        = ((*n & ~mask) | (bit << pos));
}

typedef struct _Incidence {
    ui8 rear;
    ui8 front;
    ui8 valid;
    ui8 errors;
} Incidence;

typedef struct _Incidences {
    Incidence incidences[ W_MAX ];
    ui8 nextIdx;
} Incidences;

i32 bitDiff(ui32* a, ui32* b) {
    ui32 diff = *a ^ *b;
    return __builtin_popcount(diff);
}

static inline void validateIncidences(ui8 idx, Incidences* incidences, ui32* hashTable) {
    for ( i8 i = 0, _rear; i < incidences->nextIdx; i++ ) {
        // check if not incidence start
        if ( incidences->incidences[ i ].front == idx )
            continue;
        // check if rear is in bounds
        _rear = incidences->incidences[ i ].rear - idx + incidences->incidences[ i ].front;
        if ( _rear < 0 )
            continue;
        // validate
        if ( hashTable[ idx ] != hashTable[ _rear ] ) {
            incidences->incidences[ i ].valid = 0;
            incidences->incidences[ i ].errors && (incidences->incidences[ i ].errors += 1);
        }
        if ( bitDiff(&(hashTable[ idx ]), &(hashTable[ _rear ])) == 1 )
            incidences->incidences[ i ].errors += 1;
    }
};

void algorithm(FILE* file, ui64* results) {
    ui32 rowHash = 0;
    ui32 rowHashes[ W_MAX ];
    memset(rowHashes, 0, sizeof(ui32) * W_MAX);

    ui32 colHashes[ W_MAX ];
    memset(colHashes, 0, sizeof(ui32) * W_MAX);
    ui8 colHashCount = 0;

    Incidences rowIncidences = {.nextIdx = 0};
    memset(rowIncidences.incidences, 0, sizeof(Incidence) * W_MAX);

    Incidences colIncidences = {.nextIdx = 0};
    memset(colIncidences.incidences, 0, sizeof(Incidence) * W_MAX);

    ui8 row = 0;
    ui8 col = 0;
    for ( i8 c = fgetc(file), prev_c, quit = 0;; prev_c = c, c = fgetc(file) ) {
        if ( (c == '\n' && prev_c == '\n') || quit ) {
            for ( ui8 col = 1; col < colHashCount; col++ ) {
                if ( colHashes[ col ] == colHashes[ col - 1 ] ) {
                    colIncidences.incidences[ colIncidences.nextIdx ].front   = col;
                    colIncidences.incidences[ colIncidences.nextIdx ].rear    = col - 1;
                    colIncidences.incidences[ colIncidences.nextIdx++ ].valid = 1;
                } else if ( bitDiff(&colHashes[ col ], &colHashes[ col - 1 ]) == 1 ) {
                    colIncidences.incidences[ colIncidences.nextIdx ].front    = col;
                    colIncidences.incidences[ colIncidences.nextIdx ].rear     = col - 1;
                    colIncidences.incidences[ colIncidences.nextIdx++ ].errors = 1;
                }
                validateIncidences(col, &colIncidences, colHashes);
            }

            for ( ui8 i = 0; i < colIncidences.nextIdx; i++ ) {
                if ( colIncidences.incidences[ i ].valid )
                    results[ Part1 ] += colIncidences.incidences[ i ].front;
                if ( colIncidences.incidences[ i ].errors == 1 )
                    results[ Part2 ] += colIncidences.incidences[ i ].front;
            }

            for ( ui8 i = 0; i < rowIncidences.nextIdx; i++ ) {
                if ( rowIncidences.incidences[ i ].valid )
                    results[ Part1 ] += rowIncidences.incidences[ i ].front * ROW_MULT;
                if ( rowIncidences.incidences[ i ].errors == 1 )
                    results[ Part2 ] += rowIncidences.incidences[ i ].front * ROW_MULT;
            }

            memset(rowHashes, 0, sizeof(ui32) * W_MAX);
            memset(colHashes, 0, sizeof(ui32) * W_MAX);
            memset(rowIncidences.incidences, 0, sizeof(Incidence) * W_MAX);
            memset(colIncidences.incidences, 0, sizeof(Incidence) * W_MAX);
            rowIncidences.nextIdx = colIncidences.nextIdx = row = colHashCount = 0;

            if ( quit )
                break;
            continue;
        }
        if ( (c == '\n' && prev_c != c) || c == EOF ) {
            rowHashes[ row ] = rowHash;
            if ( row ) {
                if ( rowHashes[ row ] == rowHashes[ row - 1 ] ) {
                    rowIncidences.incidences[ rowIncidences.nextIdx ].front   = row;
                    rowIncidences.incidences[ rowIncidences.nextIdx ].rear    = row - 1;
                    rowIncidences.incidences[ rowIncidences.nextIdx++ ].valid = 1;
                } else if ( bitDiff(&rowHashes[ row ], &rowHashes[ row - 1 ]) == 1 ) {
                    rowIncidences.incidences[ rowIncidences.nextIdx ].front    = row;
                    rowIncidences.incidences[ rowIncidences.nextIdx ].rear     = row - 1;
                    rowIncidences.incidences[ rowIncidences.nextIdx++ ].errors = 1;
                } else {
                    validateIncidences(row, &rowIncidences, rowHashes);
                }
            }
            colHashCount = col;
            col = rowHash = 0;
            row++;
            quit += (c == EOF);
            continue;
        }
        if ( c == '#' ) {
            modBit(&rowHash, col, 1);
            modBit(&colHashes[ col ], row, 1);
        }
        col++;
    }
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 13, &algorithm);
    return EXIT_SUCCESS;
}
