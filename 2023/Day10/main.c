#include <stdlib.h>

#include "defines.h"

typedef enum _Passage { LeftRight = '-', DownLeft = '7', DownRight = 'F', UpLeft = 'J', UpRight = 'L', UpDown = '|' } Passage;

typedef enum _Dir { None, Up, Down, Left, Right } Dir;

typedef enum _SCount { Off, On } SCount;

typedef struct _Reckon {
    ui32 line;
    ui32 col;
    Dir dir;
} Reckon;

static ui8** maze;
static ui32 cols;
static ui32 lines;

static inline void reckonStep(Reckon* reckon) {
    switch ( reckon->dir ) {
        case Up: {
            if ( reckon->line == 0 ) {
                reckon->dir = None;
                return;
            }
            switch ( maze[ reckon->line - 1 ][ reckon->col ] ) {
                case DownLeft: {
                    reckon->dir = Left;
                    break;
                }
                case DownRight: {
                    reckon->dir = Right;
                    break;
                }
                case UpDown: {
                    break;
                }
                default: {
                    reckon->dir = None;
                    return;
                }
            }
            reckon->line = reckon->line - 1;
            return;
        }
        case Down: {
            if ( reckon->line == lines - 1 ) {
                reckon->dir = None;
                return;
            }
            switch ( maze[ reckon->line + 1 ][ reckon->col ] ) {
                case UpLeft: {
                    reckon->dir = Left;
                    break;
                }
                case UpRight: {
                    reckon->dir = Right;
                    break;
                }
                case UpDown: {
                    break;
                }
                default: {
                    reckon->dir = None;
                    return;
                }
            }
            reckon->line = reckon->line + 1;
            return;
        }
        case Left: {
            if ( reckon->col == 0 ) {
                reckon->dir = 0;
                return;
            }
            switch ( maze[ reckon->line ][ reckon->col - 1 ] ) {
                case DownRight: {
                    reckon->dir = Down;
                    break;
                }
                case UpRight: {
                    reckon->dir = Up;
                    break;
                }
                case LeftRight: {
                    break;
                }
                default: {
                    reckon->dir = None;
                    return;
                }
            }
            reckon->col = reckon->col - 1;
            return;
        }
        case Right: {
            if ( reckon->col == cols - 1 ) {
                reckon->dir = 0;
                return;
            }
            switch ( maze[ reckon->line ][ reckon->col + 1 ] ) {
                case DownLeft: {
                    reckon->dir = Down;
                    break;
                }
                case UpLeft: {
                    reckon->dir = Up;
                    break;
                }
                case LeftRight: {
                    break;
                }
                default: {
                    reckon->dir = None;
                    return;
                }
            }
            reckon->col = reckon->col + 1;
        }
    };
}

void algorithm(FILE* file, ui64* results) {
    cols = 0;
    for ( i8 c = getc(file); c != '\n'; c = getc(file) )
        cols++;
    fseek(file, 0, SEEK_END);
    lines = (ftell(file) + 1) / (cols + 1);

    maze = malloc(sizeof(ui8*) * lines);
    for ( ui32 i = 0; i < lines; i++ )
        maze[ i ] = malloc(sizeof(ui8) * cols);

    Reckon start;

    rewind(file);
    ui32 col  = 0;
    ui32 line = 0;
    for ( i8 c = getc(file); c != EOF; c = getc(file) ) {
        if ( c == '\n' ) {
            line++;
            col = 0;
            continue;
        }
        if ( c == EOF ) {
            line++;
            break;
        }
        if ( c == 'S' ) {
            start.col  = col;
            start.line = line;
        }
        maze[ line ][ col++ ] = c;
    }

    Reckon reckon[ 2 ] = {{.col = start.col, .line = start.line, .dir = None}, {.col = start.col, .line = start.line, .dir = None}};
    for ( Dir d = Up, r = 0; d < 5 && (!reckon[ 0 ].dir || !reckon[ 1 ].dir); d++ ) {
        reckon[ r ].dir = d;
        reckonStep(&reckon[ r ]);
        if ( reckon[ r ].dir )
            r++;
    }

    {
        ui8 t = 0;
        for ( ui8 r = 0; r < 2; r++ )
            if ( reckon[ r ].dir == Left || reckon[ r ].dir == Down )
                t++;
        maze[ start.line ][ start.col ] = (t == 2) ? 'O' : 'I';
    }

    while ( reckon[ 0 ].dir && reckon[ 1 ].dir ) {
        for ( ui8 r = 0; r < 2; r++ ) {
            if ( !reckon[ r ].dir )
                continue;

            if ( maze[ reckon[ r ].line ][ reckon[ r ].col ] == 'O' || maze[ reckon[ r ].line ][ reckon[ r ].col ] == 'I' ) {
                reckon[ r ].dir = None;
                continue;
            }

            if ( maze[ reckon[ r ].line ][ reckon[ r ].col ] == UpLeft || maze[ reckon[ r ].line ][ reckon[ r ].col ] == UpRight ||
                 maze[ reckon[ r ].line ][ reckon[ r ].col ] == UpDown ) {
                maze[ reckon[ r ].line ][ reckon[ r ].col ] = 'I';
            } else
                maze[ reckon[ r ].line ][ reckon[ r ].col ] = 'O';

            reckonStep(&reckon[ r ]);
        }
        results[ Part1 ]++;
    }

    for ( line = 0; line < lines; line++ ) {
        SCount mode = Off;
        for ( col = 0; col < cols; col++ ) {
            if ( maze[ line ][ col ] == 'I' )
                mode = !mode;
            else if ( mode && maze[ line ][ col ] != 'O' ) {
                results[ Part2 ]++;
                // maze[line][col] = 'X';
            }
        }
    }

    // FILE* f = fopen("output.txt", "w");
    // for (line = 0; line < lines; line++) {
    //     for (col = 0; col < cols; col++) {
    //         putc(maze[line][col], f);
    //     }
    //     putc('\n', f);
    // }
    // fclose(f);

    for ( ui32 i = 0; i < lines; i++ )
        free(maze[ i ]);
    free(maze);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 10, &algorithm);
    return EXIT_SUCCESS;
}
