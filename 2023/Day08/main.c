#include <stdlib.h>
#include <string.h>

#include "defines.h"

#define A_COUNT    26
#define A_OFFSET   65 // ascii 'A' == 65
#define MAP_LENGTH 3

#define MEM_GRANULE 8

typedef enum _InstructionDir { L = 0, R = 1 } InstructionDir;
typedef enum _ReadMode { left = 0, right = 1, node = 2 } ReadMode;

typedef union _Node {
    struct _verb {
        ui8 l[ MAP_LENGTH ];
        ui8 r[ MAP_LENGTH ];
    } _v;
    ui8 _[ 2 ][ MAP_LENGTH ];
} Node;

inline static ui32 letterToIndex(i8 letter) {
    return letter - A_OFFSET;
}
inline static i8 indexToLetter(ui32 index) {
    return index + A_OFFSET;
}

void algorithm(FILE* file, ui64* results) {
    Node nodes[ A_COUNT ][ A_COUNT ][ A_COUNT ];
    memset(nodes, 0, A_COUNT * A_COUNT * A_COUNT * (sizeof(Node)));
    Node* currentNode;
    Node* targetNode;
    Node* startNode1 = NULL;

    ui32 arrSize       = MEM_GRANULE;
    Node** startNodes2 = malloc(sizeof(Node) * arrSize);
    memset(startNodes2, 0, sizeof(Node) * arrSize);
    ui32 startNodeIdx = 0;

    ui8* instructions;
    ui32 instructionCount = 0;

    ui32 line                  = 0;
    ui8 mapIdx                 = 0;
    ReadMode currentReadMode   = node;
    ui8 nodeRead[ MAP_LENGTH ] = {0, 0, 0};

    for ( i8 c = fgetc(file), prev_c;; prev_c = c, c = fgetc(file) ) {
        if ( !line ) {
            if ( c == '\n' ) {
                line++;
                rewind(file);
                instructions = malloc(instructionCount * sizeof(ui8));
                for ( ui32 i = 0; i < instructionCount; i++ ) {
                    c = fgetc(file);
                    switch ( c ) {
                        case 'L': instructions[ i ] = L; break;
                        case 'R': instructions[ i ] = R; break;
                    }
                }
            } else
                instructionCount++;
            continue;
        }

        if ( c == '(' || c == ')' || c == ',' ) {
            switch ( currentReadMode ) {
                case node:  currentReadMode = left; break;
                case left:  currentReadMode = right; break;
                case right: currentReadMode = node; break;
            }
            mapIdx = 0;
            memset(nodeRead, 0, 3);
            continue;
        }

        if ( isCapLetter(c) ) {
            switch ( currentReadMode ) {
                case node: {
                    nodeRead[ mapIdx++ ] = letterToIndex(c);
                    if ( mapIdx == MAP_LENGTH ) {
                        currentNode = &(nodes[ nodeRead[ 0 ] ][ nodeRead[ 1 ] ][ nodeRead[ 2 ] ]);

                        if ( nodeRead[ 0 ] == 25 && nodeRead[ 1 ] == 25 && nodeRead[ 2 ] == 25 )
                            targetNode = currentNode;
                        else if ( nodeRead[ 2 ] == 0 ) {
                            if ( nodeRead[ 0 ] == 0 && nodeRead[ 1 ] == 0 )
                                startNode1 = currentNode;

                            startNodes2[ startNodeIdx++ ] = currentNode;
                            if ( startNodeIdx >= arrSize ) {
                                arrSize += MEM_GRANULE;
                                startNodes2 = realloc(startNodes2, arrSize);
                            }
                        }
                    }
                    break;
                }
                case left:
                case right: currentNode->_[ currentReadMode ][ mapIdx++ ] = letterToIndex(c);
            }
            continue;
        }

        if ( c == EOF )
            break;
    }

    currentNode   = startNode1;
    ui32 instrIdx = 0;

    while ( currentNode != targetNode ) {
        ui8* nextNodeInstr = currentNode->_[ instructions[ instrIdx++ ] ];

        if ( instrIdx >= instructionCount )
            instrIdx = 0;
        currentNode = &(nodes[ nextNodeInstr[ 0 ] ][ nextNodeInstr[ 1 ] ][ nextNodeInstr[ 2 ] ]);
        results[ Part1 ]++;
    }

    i64* loopLengths = malloc(sizeof(i64) * arrSize);
    memset(loopLengths, 0, sizeof(i64) * arrSize);

    ui8* looped = malloc(sizeof(ui8) * arrSize);
    memset(looped, 0, sizeof(ui8) * arrSize);

    for ( ui32 sum = 0; sum < startNodeIdx; sum = 0 ) {
        for ( ui32 i = 0; i < startNodeIdx; i++ ) {
            if ( looped[ i ] ) {
                sum++;
                continue;
            }

            ui8* nextNodeInstr = startNodes2[ i ]->_[ instructions[ instrIdx ] ];
            loopLengths[ i ]++;
            if ( nextNodeInstr[ 2 ] == 25 ) {
                looped[ i ]++;
                sum++;
            }

            startNodes2[ i ] = &(nodes[ nextNodeInstr[ 0 ] ][ nextNodeInstr[ 1 ] ][ nextNodeInstr[ 2 ] ]);
        }

        instrIdx++;
        if ( instrIdx >= instructionCount )
            instrIdx = 0;
        if ( sum == startNodeIdx )
            break;
    }

    results[ Part2 ] = lcms_a(startNodeIdx, loopLengths);

    free(looped);
    free(loopLengths);
    free(instructions);
    free(startNodes2);
}

i32 main(i32 argc, i8* argv[]) {
    openAndRun(argc, argv, 8, &algorithm);
    return EXIT_SUCCESS;
}
